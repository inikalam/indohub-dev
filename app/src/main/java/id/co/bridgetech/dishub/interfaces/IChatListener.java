package id.co.bridgetech.dishub.interfaces;

import id.co.bridgetech.dishub.models.Chat;

public interface IChatListener {
    void chatAdded(Chat chat);
}
