package id.co.bridgetech.dishub.services;

import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.activities.LoginActivity;
import id.co.bridgetech.dishub.activities.MainActivity;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.interfaces.INotifierListener;
import id.co.bridgetech.dishub.interfaces.IPopupListener;
import id.co.bridgetech.dishub.utilities.Routes;

import static com.android.volley.Request.Method.POST;

public class ApiManager {

    protected static void execute(final int method, final String url, final Map<String,String> param, final IApiListener listener, final boolean allowRetryWhenFail, final boolean isMultipartFormData, final boolean saveToCache)
    {
        try {
            if (allowRetryWhenFail)
                BaseApplication.ACTIVITY.LOADER.setNotifierListener(new INotifierListener() {
                    @Override
                    public void onExecute() {
                        execute(method, url, param, listener, allowRetryWhenFail, isMultipartFormData,saveToCache);
                    }
                });
        }catch (Exception e){}

        listener.onStart(BaseApplication.ACTIVITY.getString(R.string.please_wait));
        String URL = Routes.ENDPOINT + url;
        StringRequest request = new StringRequest(method, URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("<!DOCTYPE")){
                            displayError(url,listener,null,allowRetryWhenFail);
                            return;
                        }
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if(obj.getBoolean("success"))
                            {
                                if(obj.getJSONObject("result") != null)
                                {
                                    JSONObject result = obj.getJSONObject("result");

                                    if(result.has("accessToken"))
                                    {
                                        listener.onSuccess(result.toString(),BaseApplication.ACTIVITY.getString(R.string.message_data_received));
                                        listener.onEnd();
                                        return;
                                    }


                                    if(result.getBoolean("forceLogin"))
                                    {
                                        PopupManager.showWithAction("Sesi telah berakhir", result.getString("message"), new IPopupListener() {
                                            @Override
                                            public void onOk() {
                                                CacheManager.clearAllCache();
                                                NavigationManager.openPage(LoginActivity.class);
                                            }
                                        });
                                    }
                                    else
                                    {
                                        Object content = null;
                                        try {
                                            content = result.get("content");
                                        }catch (Exception e){}
                                        if(content != null)
                                        {
                                            if(content.toString().equals("null"))
                                                displayError(url,listener,result.getString("message"),false);
                                            else {
                                                if(saveToCache)
                                                {
                                                    CacheManager.save(url,content.toString());
                                                }

                                                String message = result.getString("message");
                                                if(result.has("sinceId"))
                                                        message = result.getString("sinceId");

                                                listener.onSuccess(content.toString(),message );
                                                listener.onEnd();
                                            }
                                        }
                                        else
                                        {
                                            displayError(url,listener,result.getString("message"),false);
                                            return;
                                        }
                                    }
                                }
                                else
                                {
                                    displayError(url,listener,null,false);
                                    return;
                                }
                            }
                            else
                            {
                                String errorMessage = obj.getString("error");
                                displayError(url,listener,errorMessage,false);
                                return;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            displayError(url,listener,null,false);
                            return;
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errorMessage = null;
                        NetworkResponse response = error.networkResponse;

                        if(error instanceof NoConnectionError)
                        {
                            errorMessage = "Server tidak terdeteksi";
                            displayError(url,listener,errorMessage,false);
                            BaseApplication.ACTIVITY.LOADER.showNotifier(R.drawable.ic_maintenance_128dp, "Ooooopps",errorMessage);
                            return;
                        }

                        if(response != null && response.data != null){
                            switch(response.statusCode){
                                case 301:
                                    errorMessage = "Server tidak terdeteksi";
                                    displayError(url,listener,errorMessage,false);
                                    BaseApplication.ACTIVITY.LOADER.showNotifier(R.drawable.ic_maintenance_128dp, "Ooooopps",errorMessage);
                                    break;
                                case 400:
                                    errorMessage = "Koneksi anda tidak stabil, silahkan coba lagi nanti";
                                    displayError(url,listener,errorMessage,false);
                                    BaseApplication.ACTIVITY.LOADER.showNotifier(R.drawable.ic_wifi_128dp, "Ooooopps",errorMessage);
                                    break;
                                case 403:
                                    errorMessage = "Anda tidak memiliki akses untuk saat ini";
                                    displayError(url,listener,errorMessage,false);
                                    BaseApplication.ACTIVITY.LOADER.showNotifier(R.drawable.ic_maintenance_128dp, "Ooooopps",errorMessage);

                                    break;
                                case 404:
                                    errorMessage = "Server tidak terdeteksi";
                                    displayError(url,listener,errorMessage,false);
                                    BaseApplication.ACTIVITY.LOADER.showNotifier(R.drawable.ic_maintenance_128dp, "Ooooopps",errorMessage);
                                    break;
                                case 500:
                                    errorMessage = "Server kami sedang dalam perbaikan, silahkan coba lagi nanti";
                                    displayError(url,listener,errorMessage,false);
                                    BaseApplication.ACTIVITY.LOADER.showNotifier(R.drawable.ic_maintenance_128dp, "Ooooopps",errorMessage);
                                    break;
                                case 502:
                                    errorMessage = "Terjadi kesalahan jaringan";
                                    displayError(url,listener,errorMessage,false);
                                    BaseApplication.ACTIVITY.LOADER.showNotifier(R.drawable.ic_wifi_128dp, "Ooooopps",errorMessage);
                                    break;
                                case 503:
                                    errorMessage = "Server sedang maintenance, harap sabar menunggu";
                                    displayError(url,listener,errorMessage,false);
                                    BaseApplication.ACTIVITY.LOADER.showNotifier(R.drawable.ic_maintenance_128dp, "Ooooopps",errorMessage);
                                    break;
                                default:
                                    displayError(url,listener, BaseApplication.ACTIVITY.getString(R.string.message_error_connection),allowRetryWhenFail);
                                    break;
                            }
                        }
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return new Gson().toJson(param).getBytes();
            }

            @Override
            protected Map<String, String> getParams()
            {
                return param;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> header = new HashMap<>();
                if(CacheManager.getAuth()!=null)
                    header.put("Authorization" , "bearer " + CacheManager.getAuth().getAccessToken());
                if(CacheManager.isAlreadyLogin())
                    header.put("Token", CacheManager.getLocalProfile().getToken());
                if(isMultipartFormData)
                    header.put("Content-Type","multipart/form-data");
                if(!isMultipartFormData)
                    header.put("Content-Type","application/json");
                header.put("Accept","application/json");
                return header;
            }
        };
        BaseApplication.getInstance().addToRequestQueue(request);
    }

    private static void displayError(String url, IApiListener listener, String message, boolean allowRetryWhenFail)
    {
        LogManager.record(url + " FAIL : ", message == null ? BaseApplication.ACTIVITY.getString(R.string.error_no_data) : message);
        listener.onFailure(message == null ? BaseApplication.ACTIVITY.getString(R.string.error_no_data) : message);
        listener.onEnd();
        try {
            if (allowRetryWhenFail)
                BaseApplication.ACTIVITY.LOADER.showNotifier(R.mipmap.ic_launcher, "Ooooopps", message == null ? BaseApplication.ACTIVITY.getString(R.string.error_no_data) : message);
        }catch (Exception e)
        {}
    }
}
