package id.co.bridgetech.dishub.services;

import id.co.bridgetech.dishub.activities.CCTVActivity;
import id.co.bridgetech.dishub.activities.DamkarActivity;
import id.co.bridgetech.dishub.activities.DishubActivity;
import id.co.bridgetech.dishub.activities.MainActivity;
import id.co.bridgetech.dishub.activities.MapsActivity;
import id.co.bridgetech.dishub.models.Menu;

public class MenuManager {
    public static void open(Menu menu)
    {
        String screen = menu.getCode().toUpperCase();
        if(screen.equals("MAPS"))
            NavigationManager.openPage(CCTVActivity.class);
        else if(screen.equals("FIREFIGHTER"))
            NavigationManager.openPage(DamkarActivity.class);
        else if(screen.equals("HOSPITAL"))
            PopupManager.show("Pemberitahuan", "Fitur sedang dalam tahap pengembangan");
        else if(screen.equals("BICYCLE"))
            PopupManager.show("Pemberitahuan", "Fitur sedang dalam tahap pengembangan");
        else if(screen.equals("DISHUB"))
            NavigationManager.openPage(DishubActivity.class);
        else if(screen.equals("PICK_UP_STUDENT"))
        {
            /*
            let testing = await AsyncStorage.getItem('userRole');
            console.log("Helo role : ", testing)
            if(testing == 'driver')
                this.props.navigation.navigate("JemputanMapPage");
                // this.props.navigation.navigate("RegistrationRequired");
            else if(testing == 'parent')
                this.props.navigation.navigate("ParentSelectChildPage");
            else
                this.props.navigation.navigate("RegistrationRequired");*/
            PopupManager.show("Pemberitahuan", "Fitur sedang dalam tahap pengembangan");
        }
        else if(screen.equals("PICK_UP_STUDENT_PARENT"))
            PopupManager.show("Pemberitahuan", "Fitur sedang dalam tahap pengembangan");
        else if(screen.equals("WORKSERVICE"))
            PopupManager.show("Pemberitahuan", "Fitur sedang dalam tahap pengembangan");
        else if(screen.equals("SOCIAL"))
            PopupManager.show("Pemberitahuan", "Fitur sedang dalam tahap pengembangan");
        else
            PopupManager.show("Pemberitahuan", "Fitur sedang dalam tahap pengembangan");
    }
}
