package id.co.bridgetech.dishub.utilities;

import android.app.Activity;
import android.content.pm.PackageManager;

public class AppInfo {
    public static String getVersion(Activity context)
    {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(),0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
    public static String getPackage(Activity context)
    {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(),0).packageName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
