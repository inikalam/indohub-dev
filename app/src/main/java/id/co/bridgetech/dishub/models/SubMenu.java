package id.co.bridgetech.dishub.models;

import id.co.bridgetech.dishub.interfaces.ISubMenuListener;

public class SubMenu {
    private String code;
    private String name;
    private int imgResId;
    private ISubMenuListener listener;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImgResId() {
        return imgResId;
    }

    public void setImgResId(int imgResId) {
        this.imgResId = imgResId;
    }

    public ISubMenuListener getListener() {
        return listener;
    }

    public void setListener(ISubMenuListener listener) {
        this.listener = listener;
    }
}
