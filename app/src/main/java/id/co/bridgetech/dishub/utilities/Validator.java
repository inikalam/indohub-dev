package id.co.bridgetech.dishub.utilities;

import android.support.design.widget.TextInputLayout;

public class Validator {
    public static boolean isFixLengthValid(TextInputLayout control, int length)
    {
        boolean valid = control.getEditText().getText().toString().trim().length() == length;
        if(valid)return true;
        else    {
            control.setError(control.getHint().toString() + " harus terdiri dari " + length + " karakter");
            return false;
        }
    }

    public static boolean isMaxLengthValid(TextInputLayout control, int length)
    {
        boolean valid = control.getEditText().getText().toString().trim().length() <= length;
        if(valid)return true;
        else    {
            control.setError(control.getHint().toString() + " tidak boleh melebihi " + length + " karakter");
            return false;
        }
    }

    public static boolean isMinLengthValid(TextInputLayout control, int length)
    {
        boolean valid = control.getEditText().getText().toString().trim().length() >= length;
        if(valid)return true;
        else    {
            control.setError(control.getHint().toString() + " harus diisi minimal " + length + " karakter");
            return false;
        }
    }

    public static boolean isNotEmpty(TextInputLayout control)
    {
        boolean valid = control.getEditText().getText().toString().trim().length() > 0;
        if(valid)return true;
        else    {
            control.setError(control.getHint().toString() + " tidak boleh kosong");
            return false;
        }
    }

    public static boolean isSameWith(TextInputLayout control, TextInputLayout target)
    {
        boolean valid = control.getEditText().getText().toString().toLowerCase().equals(target.getEditText().getText().toString().toLowerCase());
        if(valid)return true;
        else    {
            control.setError(control.getHint().toString() + " harus sama dengan " + target.getHint().toString().toLowerCase());
            return false;
        }
    }

    public static boolean isMatchPasswordRequirements(TextInputLayout control)
    {
        boolean valid = control.getEditText().getText().toString().matches("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$");
        if(valid)return true;
        else    {
            control.setError(control.getHint().toString() + " harus mengandung huruf besar dan kecil disertai angka");
            return false;
        }
    }

    public static boolean isMailValid(TextInputLayout control)
    {
        boolean valid = control.getEditText().getText().toString().matches("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$");
        if(valid)return true;
        else    {
            control.setError(control.getHint().toString() + " format tidak sesuai");
            return false;
        }
    }
}
