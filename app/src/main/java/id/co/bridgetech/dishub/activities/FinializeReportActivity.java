package id.co.bridgetech.dishub.activities;

import android.graphics.Bitmap;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.takusemba.cropme.CropLayout;
import com.takusemba.cropme.OnCropListener;
import com.takusemba.cropme.SquareCropOverlay;

import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.utilities.Formatter;

public class FinializeReportActivity extends MasterActivity {

    byte[] image;
    Bitmap imageBitmap;
    ImageView imageReport;
    TextInputLayout layoutTitle;
    TextInputLayout layoutMessage;
    EditText inputTitle;
    EditText inputMessage;
    CropLayout cropLayout;
    SquareCropOverlay cropOverlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_finialize_report);
        initLoader();
        image = CacheManager.getTemporaryImageData("data_PHOTO");
        imageBitmap = Formatter.castToBitmap(image);

        imageReport = findViewById(R.id.image_report);
        imageReport.setImageBitmap(imageBitmap);
        imageReport.setLayoutParams(new LinearLayout.LayoutParams(DisplayManager.getDisplay().x , DisplayManager.getDisplay().x));
        //cropLayout.setBitmap(imageBitmap);
        /*cropLayout.crop(new OnCropListener() {
            @Override
            public void onSuccess(Bitmap bitmap) {
                imageReport.setImageBitmap(bitmap);
            }

            @Override
            public void onFailure() {

            }
        });*/
    }
}
