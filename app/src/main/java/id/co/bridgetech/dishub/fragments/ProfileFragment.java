package id.co.bridgetech.dishub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.activities.AboutActivity;
import id.co.bridgetech.dishub.activities.LoginActivity;
import id.co.bridgetech.dishub.activities.MasterActivity;
import id.co.bridgetech.dishub.activities.RegisterActivity;
import id.co.bridgetech.dishub.interfaces.IConfirmationListener;
import id.co.bridgetech.dishub.models.User;
import id.co.bridgetech.dishub.services.AnimationManager;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.NavigationManager;
import id.co.bridgetech.dishub.services.PopupManager;

public class ProfileFragment extends Fragment {

    MasterActivity context;
    CircleImageView imageProfile;
    User user;
    LinearLayout layoutTop;
    LinearLayout layoutEmail;
    LinearLayout layoutPhone;
    LinearLayout layoutBirthdate;
    TextView textName;
    TextView textEmail;
    TextView textPhone;
    TextView textBirthdate;
    ImageView imageEditProfile;
    ImageView imagePassword;
    ImageView imageAbout;
    ImageView imageSignout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = BaseApplication.ACTIVITY;
        user = CacheManager.getLocalProfile();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        imageProfile = view.findViewById(R.id.image_profile);
        layoutTop = view.findViewById(R.id.layout_top);
        layoutBirthdate = view.findViewById(R.id.layout_birthdate);
        layoutEmail = view.findViewById(R.id.layout_email);
        layoutPhone = view.findViewById(R.id.layout_phone);
        textName = view.findViewById(R.id.text_name);
        textBirthdate = view.findViewById(R.id.text_birthdate);
        textEmail = view.findViewById(R.id.text_email);
        textPhone = view.findViewById(R.id.text_phone);
        imageAbout = view.findViewById(R.id.image_about);
        imageEditProfile = view.findViewById(R.id.image_edit_profile);
        imagePassword = view.findViewById(R.id.image_password);
        imageSignout = view.findViewById(R.id.image_signout);

        layoutTop.setLayoutParams(new LinearLayout.LayoutParams(DisplayManager.getDisplay().x , (int) (DisplayManager.getDisplay().x*1.3)));

        textName.setText(user.getFullName());
        textPhone.setText(user.getPhoneNumber());
        textEmail.setText(user.getEmail());
        textBirthdate.setText(user.getDateOfBirth());
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.no_image);
        requestOptions.error(R.drawable.no_image);
        Glide.with(BaseApplication.ACTIVITY)
                .load(user.getImageUrl()).apply(requestOptions)
                .into(imageProfile);

        layoutPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationManager.spring(layoutPhone);
            }
        });

        layoutEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationManager.spring(layoutEmail);
            }
        });

        layoutBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimationManager.spring(layoutBirthdate);
            }
        });

        imageAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationManager.openPage(AboutActivity.class);
            }
        });

        imageSignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupManager.confirmationNotDisposeable("Konfirmasi", "Apakah kamu yakin akan keluar dari aplikasi?", new IConfirmationListener() {
                    @Override
                    public void onOk() {
                        NavigationManager.openPage(LoginActivity.class);
                        CacheManager.clearAllCache();
                        BaseApplication.ACTIVITY.finish();
                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }
        });


        return view;
    }
}
