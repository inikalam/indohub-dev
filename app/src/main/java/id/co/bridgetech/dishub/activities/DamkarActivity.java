package id.co.bridgetech.dishub.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.adapters.SubMenuAdapter;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.interfaces.ISubMenuListener;
import id.co.bridgetech.dishub.models.SubMenu;
import id.co.bridgetech.dishub.models.Tenant;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.services.NavigationManager;
import id.co.bridgetech.dishub.services.PopupManager;
import id.co.bridgetech.dishub.utilities.ListProvider;

public class DamkarActivity extends MasterActivity {

    List<SubMenu> menus;
    RecyclerView recyclerView;
    CircleImageView imageTenant;
    TextView textTitle;
    TextView textAddress;
    TextView textHour;
    Tenant data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_damkar);
        initLoader();
        menus = new ArrayList<>();

        this.imageTenant = findViewById(R.id.image_tenant);
        this.textAddress = findViewById(R.id.text_address);
        this.textTitle = findViewById(R.id.text_title);
        this.textHour = findViewById(R.id.text_hour);

        SubMenu mapMenu = new SubMenu();
        mapMenu.setCode("#1abc9c");
        mapMenu.setImgResId(R.drawable.ic_map_location_40dp);
        mapMenu.setName("Kantor dan Cabang");
        mapMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                PopupManager.showToast("Lagi Dikerjakan Peb");
//                Intent intent = new Intent();
//                CacheManager.addTemporaryData(intent,data,"data_TENANT");
//                NavigationManager.openPageWithData(DishubMapActivity.class,intent);
            }
        });
        menus.add(mapMenu);

        SubMenu postLapMenu = new SubMenu();
        postLapMenu.setCode("#1abc9c");
        postLapMenu.setImgResId(R.drawable.ic_seo);
        postLapMenu.setName("Buat Laporan");
        postLapMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                Intent intent = new Intent();
                CacheManager.addTemporaryData(intent,data,"data_TENANT");
                NavigationManager.openPageWithData(PostDamkarActivity.class,intent);
            }
        });
        menus.add(postLapMenu);

        SubMenu aboutMenu = new SubMenu();
        aboutMenu.setCode("#1abc9c");
        aboutMenu.setImgResId(R.drawable.ic_info_40dp);
        aboutMenu.setName("Tentang Kami");
        aboutMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                PopupManager.showToast("Lagi Dikerjakan Peb");
            }
        });
        menus.add(aboutMenu);

        SubMenu getLapMenu = new SubMenu();
        getLapMenu.setCode("#1abc9c");
        getLapMenu.setImgResId(R.drawable.ic_seo);
        getLapMenu.setName("Laporan Damkar");
        getLapMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                Intent intent = new Intent();
                CacheManager.addTemporaryData(intent,data,"data_TENANT");
                NavigationManager.openPageWithData(DishubMapActivity.class,intent);
            }
        });
        menus.add(getLapMenu);

        recyclerView = findViewById(R.id.recycler_view);
        SubMenuAdapter adapter = new SubMenuAdapter(menus);
        ListProvider.begin(recyclerView,adapter,4);

        HOST.getDamkarInfo(true, new IApiListener() {
            @Override
            public void onSuccess(String result, String message) {
                data = FormatManager.parseTo(Tenant.class,result);
                if(data != null)
                {
                    textAddress.setText(data.getAddress());
                    textHour.setText(data.getOpenHour() + " - " + data.getCloseHour());
                    textTitle.setText(data.getTitle());
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.no_image);
                    requestOptions.error(R.drawable.no_image);
                    Glide.with(BaseApplication.ACTIVITY)
                            .load(data.getImageUrl()).apply(requestOptions)
                            .into(imageTenant);
                }
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart(String message) {
                LOADER.show();
            }

            @Override
            public void onEnd() {
                LOADER.dismiss();
            }
        });
    }
}
