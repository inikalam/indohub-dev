package id.co.bridgetech.dishub.services;


import android.location.Location;

import com.android.volley.Request;

import java.util.HashMap;
import java.util.Map;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.utilities.Routes;

public class HostManager extends ApiManager {
    public static void initAuth(IApiListener listener)
    {
        Map<String,String> param = new HashMap<>();
        param.put("UserNameOrEmailAddress",Routes.Credential.UserNameOrEmailAddress);
        param.put("Password",Routes.Credential.Password);
        param.put("RememberClient",String.valueOf(Routes.Credential.RememberClient));
        execute(Request.Method.POST, Routes.TOKEN_AUTH,param,listener,true,false,true);
    }

    public static void login(String email, String password, IApiListener listener)
    {
        Map<String,String> param = new HashMap<>();
        param.put("email",email.toLowerCase());
        param.put("password",password);
        execute(Request.Method.POST, Routes.USER_LOGIN,param,listener,true,false,false);
    }

    public static void register(String name , String email, String password, IApiListener listener)
    {
        Map<String,String> param = new HashMap<>();
        param.put("email",email.toLowerCase());
        param.put("password",password);
        param.put("fullName",name);
        execute(Request.Method.POST, Routes.USER_LOGIN,param,listener,true,false,false);
    }

    public static void confirmEmail(String code, IApiListener listener)
    {
        Map<String,String> param = new HashMap<>();
        param.put("code",code);
        execute(Request.Method.POST, Routes.USER_CONFIRMATION_EMAIL,param,listener,true,false,false);
    }

    public static void like(String reportId, IApiListener listener)
    {
        Map<String,String> param = new HashMap<>();
        param.put("ReportId",reportId);
        execute(Request.Method.POST, Routes.REPORT_LIKE,param,listener,false,false,false);
    }

    public static void unlike(String reportId, IApiListener listener)
    {
        Map<String,String> param = new HashMap<>();
        param.put("ReportId",reportId);
        execute(Request.Method.POST, Routes.REPORT_UNLIKE,param,listener,false,false,false);
    }

    public static void getBanner(boolean loadFromCache,IApiListener listener)
    {
        if(loadFromCache)
        {
            String cache = CacheManager.load(Routes.DASHBOARD_BANER);
            if(cache != null){
                listener.onSuccess(cache,BaseApplication.ACTIVITY.getString(R.string.message_data_received));
                listener.onEnd();
                return;
            }
        }
        Map<String,String> param = new HashMap<>();
        execute(Request.Method.POST, Routes.DASHBOARD_BANER,param,listener,false,false,true);
    }

    public static void getMenu(boolean loadFromCache,IApiListener listener)
    {
        if(loadFromCache)
        {
            String cache = CacheManager.load(Routes.DASHBOARD_MENU);
            if(cache != null){
                listener.onSuccess(cache,BaseApplication.ACTIVITY.getString(R.string.message_data_received));
                listener.onEnd();
                return;
            }
        }
        Map<String,String> param = new HashMap<>();
        execute(Request.Method.POST, Routes.DASHBOARD_MENU,param,listener,false,false,true);
    }

    public static void getBroadcast(boolean loadFromCache,IApiListener listener)
    {
        if(loadFromCache)
        {
            String cache = CacheManager.load(Routes.DASHBOARD_BROADCAST);
            if(cache != null){
                listener.onSuccess(cache,BaseApplication.ACTIVITY.getString(R.string.message_data_received));
                listener.onEnd();
                return;
            }
        }
        Map<String,String> param = new HashMap<>();
        execute(Request.Method.POST, Routes.DASHBOARD_BROADCAST,param,listener,false,false,true);
    }

    public static void getDishubInfo(boolean loadFromCache,IApiListener listener)
    {
        if(loadFromCache)
        {
            String cache = CacheManager.load(Routes.DISHUB_INFO);
            if(cache != null){
                listener.onSuccess(cache,BaseApplication.ACTIVITY.getString(R.string.message_data_received));
                listener.onEnd();
                return;
            }
        }
        Map<String,String> param = new HashMap<>();
        execute(Request.Method.POST, Routes.DISHUB_INFO,param,listener,true,false,true);
    }

    public static void getDamkarInfo(boolean loadFromCache,IApiListener listener)
    {
        if(loadFromCache)
        {
            String cache = CacheManager.load(Routes.DAMKAR_INFO);
            if(cache != null){
                listener.onSuccess(cache,BaseApplication.ACTIVITY.getString(R.string.message_data_received));
                listener.onEnd();
                return;
            }
        }
        Map<String,String> param = new HashMap<>();
        execute(Request.Method.POST, Routes.DAMKAR_INFO,param,listener,true,false,true);
    }

    public static void getCCTVs(Location location, IApiListener listener)
    {
        Map<String,String> param = new HashMap<>();
        param.put("Type","0");
        param.put("Longitude", String.valueOf(location.getLongitude()));
        param.put("Latitude", String.valueOf(location.getLatitude()));
        execute(Request.Method.POST, Routes.CCTV_ALL,param,listener,true,false,false);
    }

    public static void getNotification(int page,String sinceId ,boolean loadFromCache,IApiListener listener)
    {
        if(loadFromCache)
        {
            String cache = CacheManager.load(Routes.NOTIFICATION_ALL + "?id=" + page);
            if(cache != null){
                listener.onSuccess(cache,BaseApplication.ACTIVITY.getString(R.string.message_data_received));
                listener.onEnd();
                return;
            }
        }
        Map<String,String> param = new HashMap<>();
        param.put("Limit","8");
        param.put("Page" , String.valueOf(page));
        try{
            param.put("SinceId",String.valueOf(Integer.valueOf(sinceId)));
        }catch (Exception e)
        {
            param.put("SinceId",null);
        }
        execute(Request.Method.POST, Routes.NOTIFICATION_ALL+ "?id=" + page,param,listener,true,false,true);
    }

    public static void getReports(int page,String sinceId,boolean loadFromCache,IApiListener listener)
    {
        if(loadFromCache)
        {
            String cache = CacheManager.load(Routes.REPORT_ALL + "?id=" + page);
            if(cache != null){
                listener.onSuccess(cache,BaseApplication.ACTIVITY.getString(R.string.message_data_received));
                listener.onEnd();
                return;
            }
        }
        Map<String,String> param = new HashMap<>();
        param.put("Limit","8");
        param.put("Page" , String.valueOf(page));
        param.put("Type","0");
        try{
            param.put("SinceId",String.valueOf(Integer.valueOf(sinceId)));
        }catch (Exception e)
        {
            param.put("SinceId",null);
        }

        execute(Request.Method.POST, Routes.REPORT_ALL+ "?id=" + page,param,listener,true,false,true);
    }
}
