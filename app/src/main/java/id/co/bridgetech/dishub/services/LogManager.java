package id.co.bridgetech.dishub.services;

import android.util.Log;

public class LogManager {
    public static void record(String tag, String description)
    {
        Log.d(tag,description);
    }
}
