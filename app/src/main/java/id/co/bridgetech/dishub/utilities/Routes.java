package id.co.bridgetech.dishub.utilities;

public class Routes {
    public static final String ENDPOINT = "https://www.indohub.co.id/api/";

    public static final String DASHBOARD_BANER = "Banner/GetAll/";
    public static final String DASHBOARD_BROADCAST = "Broadcast/GetBroadcastDashboard";
    public static final String DASHBOARD_MENU = "Segment/GetMenu";
    public static final String CCTV_ALL = "Cctv/GetAll";

    public static final String USER_REGISTER = "Users/Register/";
    public static final String USER_LOGIN = "Users/Login/";
    public static final String USER_LOGOUT = "Users/Logout/";
    public static final String USER_CONFIRMATION_EMAIL = "Users/ConfirmationEmail/";
    public static final String USER_RESEND_OTP = "Users/ResendOTP/";
    public static final String USER_CHANGE_PASSWORD = "Users/ChangePassword";
    public static final String USER_FORGOT_PASSWORD = "Users/ForgotPassword";
    public static final String USER_REGISTER_FCM = "Users/RegisterFCM";
    public static final String USER_REGISTER_EXPO_TOKEN = "Users/RegisterExpoToken";

    public static final String REPORT_ALL = "UserReport/GetAll";
    public static final String REPORT_CREATE = "UserReport/Create";
    public static final String REPORT_OWN = "UserReport/GetMyReport";
    public static final String REPORT_BY_ID = "UserReport/GetReportById";
    public static final String REPORT_CREATE_COMMENT = "UserReport/CreateComment";
    public static final String REPORT_LIKE = "UserReport/Like";
    public static final String REPORT_UNLIKE = "UserReport/Unlike";
    public static final String REPORT_DELETE = "UserReport/Unlike";

    public static final String DISHUB_INFO = "Dishub/GetInfo";

    public static final String DAMKAR_INFO = "Firefighter/GetInfo";
    public static final String DAMKAR_MY_REPORT = "FirefighterReport/GetMyReport";
    public static final String DAMKAR_ALL_REPORT = "FirefighterReport/GetAll";
    public static final String DAMKAR_CREATE_FEEDBACK = "FirefighterReport/Feedback";

    public static final String BIKE_ALL = "Bike/GetAll";
    public static final String BIKE_LEAVE = "Bike/GetAll";

    public static final String HOSPITAL_INFO = "Hospital/GetInfo";

    public static final String NOTIFICATION_ALL = "Notification/GetAll";

    public static final String SCHOOL_ALL = "AccompanyStudent/GetAll";
    public static final String SCHOOL_SET_STATUS = "AccompanyStudent/SetStatus";
    public static final String SCHOOL_SET_DRIVER_LOCATION = "AccompanyStudent/SetLocationDriver";

    public static final String PROFILE_OWN = "Users/GetProfile";
    public static final String PROFILE_UPDATE = "Users/GetProfile";

    public static final String PARENT_GET_STUDENT = "Parent/GetAllStudent";
    public static final String PARENT_DRIVER_LOCATION = "Parent/GetLocationDriver";
    public static final String TOKEN_AUTH = "TokenAuth/Authenticate";

    public class Credential
    {
        public static final String UserNameOrEmailAddress = "bandunghubapi@api.com";
        public static final String Password = "3}X,U8sbht=[#H";
        public static final boolean RememberClient = true;
    }
}
