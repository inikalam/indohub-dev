package id.co.bridgetech.dishub.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.models.Notification;
import id.co.bridgetech.dishub.services.ApiManager;
import id.co.bridgetech.dishub.viewholders.NotificationViewHolder;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationViewHolder> {
    private List<Notification> data;

    public NotificationAdapter(List<Notification> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_notification, viewGroup, false);
        return new NotificationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final NotificationViewHolder notificationViewHolder, final int i) {
        final Notification item = data.get(i);
        if(item != null) {
            notificationViewHolder.getTextDate().setText(item.getCreationTime());
            notificationViewHolder.getTextMessage().setText(item.getMessage());
            notificationViewHolder.getTextDate().setText(item.getDate());
            notificationViewHolder.getTextTime().setText(item.getTime());
            notificationViewHolder.getTextTitle().setText(item.getTitle());

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.no_image);
            requestOptions.error(R.drawable.no_image);
            Glide.with(BaseApplication.ACTIVITY)
                    .load(item.getImageUrl()).apply(requestOptions)
                    .into(notificationViewHolder.getImageIcon());

            notificationViewHolder.getLayoutRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            }); }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
