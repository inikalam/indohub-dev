package id.co.bridgetech.dishub.activities;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.interfaces.IInfoWindowClickListener;
import id.co.bridgetech.dishub.models.Tenant;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.utilities.BitmapBuilder;

public class DishubMapActivity extends MapsActivity {

    Tenant data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_cctv);
        initLoader();
        data = CacheManager.getTemporaryData("data_TENANT",Tenant.class);

        setupMap(savedInstanceState);
        setListener(new IInfoWindowClickListener() {
            @Override
            public void openMarker(Marker marker) {
            }
        });
    }
    @Override
    void loadData() {
        MarkerOptions opt = new MarkerOptions();
        opt.position(new LatLng(data.getListTenant().get(0).getCoordinate().getLatitude(),data.getListTenant().get(0).getCoordinate().getLongitude()));
        opt.flat(true);
        opt.snippet(data.getAddress());
        opt.title(data.getTitle());
        opt.icon(BitmapDescriptorFactory.fromBitmap(BitmapBuilder.createMarkerFromUrl(data.getImageUrl())));
        Marker marker = gMap.addMarker(opt);
        marker.setTag(data);
        moveCameraTo(marker);
    }
}
