package id.co.bridgetech.dishub.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.models.User;
import id.co.bridgetech.dishub.services.CacheManager;

public class VerificationActivity extends MasterActivity {

    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        user = CacheManager.getTemporaryData("data_USER",User.class);

    }
}
