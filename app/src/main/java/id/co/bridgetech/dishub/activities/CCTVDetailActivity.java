package id.co.bridgetech.dishub.activities;

import android.net.Uri;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.List;

import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.adapters.ChatAdapter;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.interfaces.IChatListener;
import id.co.bridgetech.dishub.interfaces.IPullToRefreshListener;
import id.co.bridgetech.dishub.models.Broadcast;
import id.co.bridgetech.dishub.models.CCTV;
import id.co.bridgetech.dishub.models.Chat;
import id.co.bridgetech.dishub.models.Notification;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.services.MediaPlayerManager;
import id.co.bridgetech.dishub.utilities.ListProvider;

public class CCTVDetailActivity extends MasterActivity {

    SimpleExoPlayerView video;
    CCTV cctv;
    MediaPlayerManager videoManager;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout layoutRefresh;
    private List<Chat> chats;
    private ChatAdapter chatAdapter;
    private LinearLayout layoutBroadcast;
    private TextView textBroadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_cctvdetail);
        initLoader();
        initEmpty();

        recyclerView = findViewById(R.id.recycler_view);
        layoutRefresh = findViewById(R.id.layout_refresh);
        layoutBroadcast = findViewById(R.id.layout_broadcast);
        textBroadcast = findViewById(R.id.text_broadcast);

        chats = new ArrayList<>();
        chatAdapter = new ChatAdapter(chats);

        ListProvider.begin(recyclerView,chatAdapter,1);
        ListProvider.setPullToRefresh(layoutRefresh, new IPullToRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

        video = findViewById(R.id.player_view);
        cctv = CacheManager.getTemporaryData("data_CCTV",CCTV.class);
        video.setLayoutParams(new LinearLayout.LayoutParams(DisplayManager.getDisplay().x , (int) (DisplayManager.getDisplay().x*0.7)));
        videoManager = new MediaPlayerManager(video,cctv);
        loadData();
        EMPTY.show(R.drawable.ic_chat_128dp,"Belum ada perbincangan","Ini saatnya kamu yang memulai, tulislah pesanmu sekarang");
    }

    void loadData()
    {
        chats.clear();
        CHAT.initChat(cctv.getId(), new IChatListener() {
            @Override
            public void chatAdded(Chat chat) {
                try {
                    chats.add(chat);
                    chatAdapter.notifyDataSetChanged();

                    recyclerView.scrollToPosition(chats.size() - 1);
                }catch (Exception e){}
                if(chats.size() > 0)
                    EMPTY.dismiss();
            }
        });

        HOST.getBroadcast(false, new IApiListener() {
            @Override
            public void onSuccess(String result, String message) {
                Broadcast data = FormatManager.parseTo(Broadcast.class,result);
                textBroadcast.setText(data.getContent());
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart(String message) {
                if(!layoutRefresh.isRefreshing())
                    LOADER.show();
            }

            @Override
            public void onEnd() {
                layoutRefresh.setRefreshing(false);
                LOADER.dismiss();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoManager.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoManager.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        videoManager.stop();
    }
}
