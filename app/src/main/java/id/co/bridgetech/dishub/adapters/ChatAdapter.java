package id.co.bridgetech.dishub.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.models.Chat;
import id.co.bridgetech.dishub.models.Notification;
import id.co.bridgetech.dishub.viewholders.ChatViewHolder;
import id.co.bridgetech.dishub.viewholders.NotificationViewHolder;

public class ChatAdapter extends RecyclerView.Adapter<ChatViewHolder> {

    private List<Chat> data;

    public ChatAdapter(List<Chat> data) {
        this.data = data;
    }


    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_chat, viewGroup, false);
        return new ChatViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder chatViewHolder, int i) {
        final Chat item = data.get(i);
        if(item != null) {
            chatViewHolder.getTextMessage().setText(item.getText());
            chatViewHolder.getTextDate().setText(item.getDate() + " " + item.getTime());
            try {
                chatViewHolder.getTextName().setText(item.getUser().getFullName());
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.no_image);
            requestOptions.error(R.drawable.no_image);
            Glide.with(BaseApplication.ACTIVITY)
                    .load(item.getUser().getImageUrl()).apply(requestOptions)
                    .into(chatViewHolder.getImageProfile());
            }catch (Exception e){}
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
