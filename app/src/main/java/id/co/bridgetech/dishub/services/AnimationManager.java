package id.co.bridgetech.dishub.services;

import android.animation.ValueAnimator;
import android.view.View;

import com.appolica.flubber.Flubber;

public class AnimationManager {
    public static void slideInRight(View view)
    {
        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_RIGHT)
                .duration(1000)
                .createFor(view).start();
    }
    public static void slideInRightWithDelay(View view,Long delay)
    {
        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_RIGHT).delay(delay)
                .duration(1000)
                .createFor(view).start();
    }

    public static void slideInLeft(View view)
    {
        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_LEFT)
                .duration(1000)
                .createFor(view).start();
    }

    public static void slideInLeftWithDelay(View view,Long delay)
    {
        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_LEFT).delay(delay)
                //.interpolator(Flubber.Curve.SPRING)
                .duration(1000)//.repeatMode(ValueAnimator.REVERSE)
                .createFor(view).start();
    }

    public static void slideInUp(View view)
    {
        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_UP)
                .duration(1000)
                .createFor(view).start();
    }

    public static void slideInUpWithDelay(View view,Long delay)
    {
        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_UP).delay(delay)
                .duration(1000)
                .createFor(view).start();
    }

    public static void spring(View view)
    {
        Flubber.with()
                .animation(Flubber.AnimationPreset.MORPH)
                .interpolator(Flubber.Curve.SPRING)
                .duration(1000).repeatMode(ValueAnimator.REVERSE)
                .createFor(view).start();
    }

    public static void springWithDelay(View view, Long delay)
    {
        Flubber.with()
                .animation(Flubber.AnimationPreset.MORPH)
                .interpolator(Flubber.Curve.SPRING).delay(delay)
                .duration(1000).repeatMode(ValueAnimator.REVERSE)
                .createFor(view).start();
    }

    public static void pinch(View view)
    {
        Flubber.with()
                .animation(Flubber.AnimationPreset.ZOOM_IN)
                .interpolator(Flubber.Curve.SPRING)
                .duration(500).repeatMode(ValueAnimator.REVERSE).repeatCount(999999)
                .createFor(view).start();
    }
}
