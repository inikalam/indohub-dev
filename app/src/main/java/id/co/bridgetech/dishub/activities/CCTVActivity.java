package id.co.bridgetech.dishub.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.co.bridgetech.dishub.BuildConfig;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.adapters.InfoWindowAdapter;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.interfaces.IInfoWindowClickListener;
import id.co.bridgetech.dishub.models.CCTV;
import id.co.bridgetech.dishub.models.Report;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.services.NavigationManager;
import id.co.bridgetech.dishub.services.PopupManager;
import id.co.bridgetech.dishub.utilities.BitmapBuilder;

public class CCTVActivity extends MapsActivity {

    private List<CCTV> cctvs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_cctv);
        initLoader();
        cctvs = new ArrayList<>();
        setupMap(savedInstanceState);
        setListener(new IInfoWindowClickListener() {
            @Override
            public void openMarker(Marker marker) {
                CCTV item = (CCTV) marker.getTag();
                if(item != null)
                {
                    Intent intent = new Intent();
                    CacheManager.addTemporaryData(intent,item,"data_CCTV");
                    NavigationManager.openPageWithData(CCTVDetailActivity.class,intent);
                }
            }
        });
    }

    @Override
    void loadData() {
        HOST.getCCTVs(myLocation(), new IApiListener() {
            @Override
            public void onSuccess(String result, String message) {
                List<CCTV> data = FormatManager.parseTo(new TypeToken<List<CCTV>>(){}.getType(),result);
                if(data != null) {
                    for (CCTV item : data) {
                        cctvs.add(item);
                        MarkerOptions opt = new MarkerOptions();
                        opt.position(new LatLng(item.getCoordinate().getLatitude(),item.getCoordinate().getLongitude()));
                        opt.flat(true);
                        opt.snippet(item.getAddress());
                        opt.title(item.getTitle());
                        opt.icon(BitmapDescriptorFactory.fromBitmap(BitmapBuilder.createMarker(R.drawable.ic_cctv_36dp)));
                        Marker marker = gMap.addMarker(opt);
                        marker.setTag(item);
                    }
                }

            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart(String message) {

            }

            @Override
            public void onEnd() {
            }
        });
    }
}

