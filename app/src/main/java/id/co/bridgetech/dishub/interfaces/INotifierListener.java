package id.co.bridgetech.dishub.interfaces;

public interface INotifierListener {
    void onExecute();
}
