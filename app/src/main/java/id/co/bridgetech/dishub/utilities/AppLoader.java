package id.co.bridgetech.dishub.utilities;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tuyenmonkey.mkloader.MKLoader;

import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.interfaces.INotifierListener;
import id.co.bridgetech.dishub.services.AnimationManager;
import id.co.bridgetech.dishub.services.LogManager;

public class AppLoader {
    private Activity context;
    private RelativeLayout overlay;
    private TextView textTitle;
    private TextView textMessage;
    private LinearLayout layoutImage;
    private ImageView imagePresenter;
    private INotifierListener notifierListener;
    private TextView textRetry;
    private MKLoader loader;

    public INotifierListener getNotifierListener() {
        return notifierListener;
    }

    public void setNotifierListener(final INotifierListener notifierListener) {
        this.notifierListener = notifierListener;
        this.textRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifierListener.onExecute();
                dismissNotifier();
                show();
            }
        });
    }

    public AppLoader(Activity context) {
        this.context = context;
        this.overlay = context.findViewById(R.id.progress_overlay);
        this.imagePresenter = context.findViewById(R.id.image_presenter);
        this.textMessage = context.findViewById(R.id.text_message_notifier);
        this.textTitle = context.findViewById(R.id.text_title_notifier);
        this.layoutImage = context.findViewById(R.id.layout_image_notifier);
        this.textRetry = context.findViewById(R.id.text_retry);
        this.loader = context.findViewById(R.id.loader_notifier);
        dismiss();
        dismissNotifier();
    }

    public void showNotifier(int imageResId, String title , String message)
    {
        try {
            this.imagePresenter.setImageDrawable(context.getDrawable(imageResId));
            this.textTitle.setText(title);
            this.textMessage.setText(message);
            this.layoutImage.setVisibility(View.VISIBLE);
            dismiss();
            this.overlay.setVisibility(View.VISIBLE);
            AnimationManager.spring(imagePresenter);
        }catch (Exception ex){
            LogManager.record("SHOW_NOTIFIER",ex.getMessage());
        }
    }

    public void dismissNotifier()
    {
        try {
            this.layoutImage.setVisibility(View.INVISIBLE);
            this.overlay.setVisibility(View.INVISIBLE);
        }catch (Exception ex){}
    }

    public void show(){
        try {
            loader.setVisibility(View.VISIBLE);
            AnimationManager.spring(loader);
            dismissNotifier();
            overlay.setVisibility(View.VISIBLE);
            //overlay.bringToFront();
        }catch (Exception ex){}
    }
    public void dismiss(){
        try {
            overlay.setVisibility(View.INVISIBLE);
            loader.setVisibility(View.INVISIBLE);
        }catch (Exception ex){}
    }
}
