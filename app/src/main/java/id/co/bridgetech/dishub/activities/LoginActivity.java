package id.co.bridgetech.dishub.activities;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.models.User;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.services.NavigationManager;
import id.co.bridgetech.dishub.services.PopupManager;
import id.co.bridgetech.dishub.utilities.AppInfo;
import id.co.bridgetech.dishub.utilities.Validator;

public class LoginActivity extends MasterActivity {

    TextInputLayout layoutEmail;
    TextInputLayout layoutPassword;

    Button buttonLogin;
    TextView textRegister;
    TextView textAppVersion;

    EditText inputEmail;
    EditText inputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_login);
        initLoader();

        this.layoutEmail = findViewById(R.id.input_email);
        this.layoutPassword = findViewById(R.id.input_password);
        this.inputEmail = findViewById(R.id.text_email);
        this.inputPassword = findViewById(R.id.text_password);
        this.buttonLogin = findViewById(R.id.button_signin);
        this.textAppVersion = findViewById(R.id.app_version);
        this.textRegister = findViewById(R.id.text_register);

        textAppVersion.setText("App version v." + AppInfo.getVersion(this));

        layoutPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                removeError();
                return false;
            }
        });

        layoutEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                removeError();
                return false;
            }
        });

        textRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationManager.openPage(RegisterActivity.class);
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeError();
                if(!Validator.isNotEmpty(layoutEmail))return;
                if(!Validator.isMinLengthValid(layoutEmail,4))return;
                if(!Validator.isMaxLengthValid(layoutEmail,25))return;
                if(!Validator.isNotEmpty(layoutPassword))return;
                if(!Validator.isMinLengthValid(layoutPassword,6))return;
                if(!Validator.isMaxLengthValid(layoutPassword,25))return;

                HOST.login(inputEmail.getText().toString(), inputPassword.getText().toString(), new IApiListener() {
                    @Override
                    public void onSuccess(String result, String message) {
                        User data = FormatManager.parseTo(User.class,result);
                        if(data.isConfirmed()) {
                            CacheManager.saveLocalProfile(data);
                            PopupManager.createAnnouncement("Selamat Datang", message);
                            NavigationManager.openPage(MainActivity.class);
                            finish();
                        }
                        else
                        {
                            Intent intent = new Intent();
                            CacheManager.addTemporaryData(intent,data,"data_USER");
                            NavigationManager.openPageWithData(VerificationActivity.class,intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        PopupManager.showToast(message);
                    }

                    @Override
                    public void onStart(String message) {
                        LOADER.show();
                    }

                    @Override
                    public void onEnd() {
                        LOADER.dismiss();
                    }
                });
            }
        });
    }

    private void removeError()
    {
        inputEmail.setError(null);
        inputPassword.setError(null);
        layoutEmail.setError(null);
        layoutPassword.setError(null);
    }

    @Override
    public void onBackPressed() {

    }
}
