package id.co.bridgetech.dishub.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.interfaces.INotifierListener;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.NetworkManager;

public class WebActivity extends MasterActivity {

    WebView webView;
    String link = "www.google.com";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_web);
        initLoader();
        this.webView = findViewById(R.id.webview);
        this.link = CacheManager.getTemporaryData("data_LINK");
        LOADER.setNotifierListener(new INotifierListener() {
            @Override
            public void onExecute() {
                load();
            }
        });
        load();
    }

    void load()
    {
        if(NetworkManager.hasInternetConnection()) {
            try {
                LOADER.show();
                webView.getSettings().setJavaScriptEnabled(true);
                webView.setWebChromeClient(new WebChromeClient());
                webView.getSettings().setDomStorageEnabled(true);
                webView.loadUrl(link);
                webView.setWebViewClient(new WebViewClient(){

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        LOADER.dismiss();
                    }
                });
            } catch (Exception e) {

            }
        }
        else
        {
            LOADER.showNotifier(R.drawable.ic_wifi_128dp,"Tidak ada koneksi","Silahkan cek koneksi internet anda lalu coba lagi");
        }
    }
}
