package id.co.bridgetech.dishub.interfaces;

public interface IApiListener {
    void onSuccess(String result, String message);
    void onFailure(String message);
    void onStart(String message);
    void onEnd();
}
