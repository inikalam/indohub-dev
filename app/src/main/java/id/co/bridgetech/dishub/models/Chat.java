package id.co.bridgetech.dishub.models;

import com.bumptech.glide.annotation.Excludes;

import id.co.bridgetech.dishub.utilities.Formatter;

public class Chat {
    private String text;
    private Object timestamp;
    private FirebaseUser user;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Object getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Object timestamp) {
        this.timestamp = timestamp;
    }

    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(FirebaseUser user) {
        this.user = user;
    }

    public String getDate()
    {
        try{
            return Formatter.castToFormattedDate(Long.parseLong(timestamp.toString()));
        }
        catch (Exception e)
        {
            return "";
        }
    }

    public String getTime()
    {
        try{
            return Formatter.castToFormattedTime(Long.parseLong(timestamp.toString()));
        }
        catch (Exception e)
        {
            return "";
        }
    }

    public static class FirebaseUser
    {
        private String imageUrl;
        private String fullName;

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }
    }
}
