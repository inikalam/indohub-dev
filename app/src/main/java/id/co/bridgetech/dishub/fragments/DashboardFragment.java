package id.co.bridgetech.dishub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.reflect.TypeToken;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.synnapps.carouselview.ViewListener;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.activities.MasterActivity;
import id.co.bridgetech.dishub.adapters.MenuAdapter;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.interfaces.IEndlessScrollListener;
import id.co.bridgetech.dishub.interfaces.IPullToRefreshListener;
import id.co.bridgetech.dishub.models.Banner;
import id.co.bridgetech.dishub.models.Broadcast;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.utilities.ListProvider;

public class DashboardFragment extends Fragment {
    private RecyclerView recyclerView;
    private SwipeRefreshLayout layoutRefresh;
    private List<Banner> banner;
    private List<id.co.bridgetech.dishub.models.Menu> menu;
    private MasterActivity context;
    private CarouselView slider;
    private MenuAdapter menuAdapter;
    private LinearLayout layoutBroadcast;
    private ImageView imageBroadcast;
    private TextView textBroadcast;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        banner = new ArrayList<>();
        menu = new ArrayList<>();
        context = BaseApplication.ACTIVITY;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        layoutRefresh = view.findViewById(R.id.layout_refresh);
        recyclerView = view.findViewById(R.id.recycler_view);
        slider = view.findViewById(R.id.banner);
        imageBroadcast = view.findViewById(R.id.image_broadcast);
        textBroadcast = view.findViewById(R.id.text_broadcast);
        layoutBroadcast = view.findViewById(R.id.layout_broadcast);

        menuAdapter = new MenuAdapter(menu);
        ListProvider.begin(recyclerView,menuAdapter,3);

        ListProvider.setPullToRefresh(layoutRefresh, new IPullToRefreshListener() {
            @Override
            public void onRefresh() {
                loadAll(false);
            }
        });
        loadAll(true);

        layoutRefresh.setLayoutParams(new LinearLayout.LayoutParams(DisplayManager.getDisplay().x , (int) (DisplayManager.getDisplay().x*0.7)));
        slider.setLayoutParams(new LinearLayout.LayoutParams(DisplayManager.getDisplay().x , (int) (DisplayManager.getDisplay().x*0.7)));
        //layoutBroadcast.setVisibility(View.INVISIBLE);
        return view;
    }

    private void loadAll(boolean loadFromCache)
    {
        loadBroadcast(loadFromCache);
        loadBanner(loadFromCache);
        loadMenu(loadFromCache);
    }

    private void loadBroadcast(boolean loadFromCache)
    {
        context.HOST.getBroadcast(loadFromCache, new IApiListener() {
            @Override
            public void onSuccess(String result, String message) {
                Broadcast data = FormatManager.parseTo(Broadcast.class,result);
                layoutBroadcast.setVisibility(View.VISIBLE);
                textBroadcast.setText(data.getContent());
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart(String message) {

            }

            @Override
            public void onEnd() {
                layoutRefresh.setRefreshing(false);
            }
        });
    }

    private void loadMenu(boolean loadFromCache)
    {
        context.HOST.getMenu(loadFromCache, new IApiListener() {
            @Override
            public void onSuccess(String result, String message) {
                menu.clear();
                List<id.co.bridgetech.dishub.models.Menu> data = FormatManager.parseTo(new TypeToken<List<id.co.bridgetech.dishub.models.Menu>>(){}.getType(),result);
                if(data != null)
                    for(id.co.bridgetech.dishub.models.Menu item : data) {
                        menu.add(item);
                    }
                menuAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart(String message) {

            }

            @Override
            public void onEnd() {
                layoutRefresh.setRefreshing(false);
            }
        });
    }

    private void loadBanner(boolean loadFromCache)
    {
        context.HOST.getBanner(loadFromCache, new IApiListener() {
            @Override
            public void onSuccess(String result, String message) {
                banner.clear();
                List<Banner> data = FormatManager.parseTo(new TypeToken<List<Banner>>(){}.getType(),result);
                if(data != null)
                    for(Banner item : data) {
                        banner.add(item);
                    }
                slider.setImageListener(imageListener);
                slider.setPageCount(banner.size());
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart(String message) {

            }

            @Override
            public void onEnd() {
                layoutRefresh.setRefreshing(false);
            }
        });
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.no_image);
            requestOptions.error(R.drawable.no_image);
            requestOptions.downsample(new DownsampleStrategy() {
                @Override
                public float getScaleFactor(int sourceWidth, int sourceHeight, int requestedWidth, int requestedHeight) {
                    return 0.4f;
                }

                @Override
                public SampleSizeRounding getSampleSizeRounding(int sourceWidth, int sourceHeight, int requestedWidth, int requestedHeight) {
                    return null;
                }
            });
            Glide.with(BaseApplication.ACTIVITY)
                    .load(banner.get(position).getImageUrl()).apply(requestOptions)
                    .into(imageView);
        }
    };
}
