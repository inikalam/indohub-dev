package id.co.bridgetech.dishub.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.services.ApiManager;
import id.co.bridgetech.dishub.services.ChatManager;
import id.co.bridgetech.dishub.services.HostManager;
import id.co.bridgetech.dishub.utilities.AppLoader;
import id.co.bridgetech.dishub.utilities.LayoutEmpty;

public class MasterActivity extends AppCompatActivity {
    public LayoutEmpty EMPTY;
    public AppLoader LOADER;
    public HostManager HOST;
    public ChatManager CHAT;
    public void initEmpty(View view)
    {
        EMPTY = new LayoutEmpty(view);
    }
    public void initEmpty()
    {
        EMPTY = new LayoutEmpty();
    }
    protected void initLoader()
    {
        LOADER = new AppLoader(this);
    }
    protected void initHost() { HOST = new HostManager(); }
    protected void initActivity()
    {
        BaseApplication.ACTIVITY = this;
    }
    protected void initChat(){ CHAT = new ChatManager(); }

    @Override
    protected void onResume() {
        super.onResume();
        initActivity();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivity();
        initHost();
        initChat();
    }
}
