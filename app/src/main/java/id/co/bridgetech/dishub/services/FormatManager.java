package id.co.bridgetech.dishub.services;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public class FormatManager {
    public static <T> T parseTo(Class<T> type,String json)
    {
        try
        {
            return new Gson().fromJson(json,type);
        }catch (Exception e)
        {
            return  null;
        }
    }

    public static <T> T parseTo(Type type, String json)
    {
        try
        {
            return new Gson().fromJson(json,type);
        }catch (Exception e)
        {
            return  null;
        }
    }
}
