package id.co.bridgetech.dishub.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.services.AnimationManager;
import id.co.bridgetech.dishub.services.MenuManager;
import id.co.bridgetech.dishub.viewholders.MenuViewHolder;

public class MenuAdapter extends RecyclerView.Adapter<MenuViewHolder> {

    private List<id.co.bridgetech.dishub.models.Menu> data = new ArrayList<>();
    public MenuAdapter(List<id.co.bridgetech.dishub.models.Menu> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_menu, viewGroup, false);
        return new MenuViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MenuViewHolder menuViewHolder, int i) {
        final id.co.bridgetech.dishub.models.Menu item = data.get(i);

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.no_image);
            requestOptions.error(R.drawable.no_image);
            Glide.with(BaseApplication.ACTIVITY)
                    .load(item.getImageUrl()).apply(requestOptions)
                    .into(menuViewHolder.getImageThumbnail());
        menuViewHolder.getTextTitle().setText(item.getName());
        menuViewHolder.getLayoutRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuManager.open(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
