package id.co.bridgetech.dishub.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.adapters.SubMenuAdapter;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.interfaces.ISubMenuListener;
import id.co.bridgetech.dishub.models.Menu;
import id.co.bridgetech.dishub.models.SubMenu;
import id.co.bridgetech.dishub.models.Tenant;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.services.NavigationManager;
import id.co.bridgetech.dishub.utilities.Formatter;
import id.co.bridgetech.dishub.utilities.ListProvider;

public class DishubActivity extends MasterActivity {

    List<SubMenu> menus;
    RecyclerView recyclerView;
    CircleImageView imageTenant;
    TextView textTitle;
    TextView textAddress;
    TextView textHour;
    Tenant data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_dishub);
        initLoader();
        menus = new ArrayList<>();

        this.imageTenant = findViewById(R.id.image_tenant);
        this.textAddress = findViewById(R.id.text_address);
        this.textTitle = findViewById(R.id.text_title);
        this.textHour = findViewById(R.id.text_hour);

        SubMenu mapMenu = new SubMenu();
        mapMenu.setCode("#1abc9c");
        mapMenu.setImgResId(R.drawable.ic_map_location_40dp);
        mapMenu.setName("Peta Layanan");
        mapMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                Intent intent = new Intent();
                CacheManager.addTemporaryData(intent,data,"data_TENANT");
                NavigationManager.openPageWithData(DishubMapActivity.class,intent);
            }
        });
        menus.add(mapMenu);

        SubMenu aboutMenu = new SubMenu();
        aboutMenu.setCode("#1abc9c");
        aboutMenu.setImgResId(R.drawable.ic_info_40dp);
        aboutMenu.setName("Tentang Kami");
        aboutMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                NavigationManager.openPage(CCTVActivity.class);
            }
        });
        menus.add(aboutMenu);

        SubMenu webMenu = new SubMenu();
        webMenu.setCode("#1abc9c");
        webMenu.setImgResId(R.drawable.ic_seo);
        webMenu.setName("Website Kami");
        webMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                Intent intent = new Intent();
                CacheManager.addTemporaryData(intent,"http://dishub.bandung.go.id","data_LINK");
                NavigationManager.openPageWithData(WebActivity.class,intent);
            }
        });
        menus.add(webMenu);

        SubMenu bikeMenu = new SubMenu();
        bikeMenu.setCode("#1abc9c");
        bikeMenu.setImgResId(R.drawable.ic_bicycle_40dp);
        bikeMenu.setName("HUB Bike");
        bikeMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                NavigationManager.openPage(CCTVActivity.class);
            }
        });
        menus.add(bikeMenu);

        recyclerView = findViewById(R.id.recycler_view);
        SubMenuAdapter adapter = new SubMenuAdapter(menus);
        ListProvider.begin(recyclerView,adapter,4);

        HOST.getDishubInfo(true, new IApiListener() {
            @Override
            public void onSuccess(String result, String message) {
                data = FormatManager.parseTo(Tenant.class,result);
                if(data != null)
                {
                    textAddress.setText(data.getAddress());
                    textHour.setText(data.getOpenHour() + " - " + data.getCloseHour());
                    textTitle.setText(data.getTitle());
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.no_image);
                    requestOptions.error(R.drawable.no_image);
                    Glide.with(BaseApplication.ACTIVITY)
                            .load(data.getImageUrl()).apply(requestOptions)
                            .into(imageTenant);
                }
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart(String message) {
                LOADER.show();
            }

            @Override
            public void onEnd() {
                LOADER.dismiss();
            }
        });
    }
}
