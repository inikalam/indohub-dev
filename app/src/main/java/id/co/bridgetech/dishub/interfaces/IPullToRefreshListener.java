package id.co.bridgetech.dishub.interfaces;

public interface IPullToRefreshListener {
    void onRefresh();
}
