package id.co.bridgetech.dishub.interfaces;

public interface IConfirmationListener {
    void onOk();
    void onCancel();
}
