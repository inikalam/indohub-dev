package id.co.bridgetech.dishub.adapters;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.models.Notification;
import id.co.bridgetech.dishub.models.Report;
import id.co.bridgetech.dishub.services.AnimationManager;
import id.co.bridgetech.dishub.viewholders.NotificationViewHolder;
import id.co.bridgetech.dishub.viewholders.ReportViewHolder;

public class ReportAdapter extends RecyclerView.Adapter<ReportViewHolder> {
    private List<Report> data;

    public ReportAdapter(List<Report> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_report, viewGroup, false);
        return new ReportViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ReportViewHolder reportViewHolder, final int i) {
        final Report item = data.get(i);
        if(item != null) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.no_image);
            requestOptions.error(R.drawable.no_image);
            Glide.with(BaseApplication.ACTIVITY)
                    .load(item.getImageUrl()).apply(requestOptions)
                    .into(reportViewHolder.getImagePosting());
            Glide.with(BaseApplication.ACTIVITY)
                    .load(item.getUser().getImageUrl()).apply(requestOptions)
                    .into(reportViewHolder.getImageProfile());

            reportViewHolder.getTextLocation().setText(item.getAddress());
            reportViewHolder.getTextTime().setText(item.getTime());
            reportViewHolder.getTextDate().setText(item.getDate());
            reportViewHolder.getTextName().setText(item.getUser().getFullName());
            reportViewHolder.getTextComment().setText(item.getCommentCount());
            reportViewHolder.getTextLike().setText(item.getLikeCount());
            reportViewHolder.getTextDescription().setText(item.getDescription());
            reportViewHolder.getImageLike().setImageDrawable(item.isLike() ? BaseApplication.ACTIVITY.getResources().getDrawable(R.drawable.ic_heart_active_36do) : BaseApplication.ACTIVITY.getResources().getDrawable(R.drawable.ic_heart_inactive_36dp) );

            reportViewHolder.getLayoutLocation().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?f=d&daddr="+item.getAddress()));
                    intent.setComponent(new ComponentName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity"));
                    if (intent.resolveActivity(BaseApplication.ACTIVITY.getPackageManager()) != null) {
                        BaseApplication.ACTIVITY.startActivity(intent);
                    }
                }
            });

            reportViewHolder.getImageComment().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AnimationManager.spring(reportViewHolder.getImageComment());
                }
            });

            reportViewHolder.getImageLike().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.setLike(!item.isLike());

                    if(item.isLike())
                    {
                        item.setLikeCount(String.valueOf(Integer.valueOf(item.getLikeCount())+1));
                        BaseApplication.ACTIVITY.HOST.like(item.getId(), new IApiListener() {
                            @Override
                            public void onSuccess(String result, String message) {

                            }

                            @Override
                            public void onFailure(String message) {

                            }

                            @Override
                            public void onStart(String message) {

                            }

                            @Override
                            public void onEnd() {

                            }
                        });
                    }
                    else
                    {
                        item.setLikeCount(String.valueOf(Integer.valueOf(item.getLikeCount())-1));
                        BaseApplication.ACTIVITY.HOST.unlike(item.getId(), new IApiListener() {
                            @Override
                            public void onSuccess(String result, String message) {
                            }

                            @Override
                            public void onFailure(String message) {

                            }

                            @Override
                            public void onStart(String message) {

                            }

                            @Override
                            public void onEnd() {

                            }
                        });
                    }

                    AnimationManager.spring(reportViewHolder.getImageLike());
                    reportViewHolder.getImageLike().setImageDrawable(item.isLike() ? BaseApplication.ACTIVITY.getResources().getDrawable(R.drawable.ic_heart_active_36do) : BaseApplication.ACTIVITY.getResources().getDrawable(R.drawable.ic_heart_inactive_36dp) );
                    reportViewHolder.getTextComment().setText(item.getCommentCount());
                    reportViewHolder.getTextLike().setText(item.getLikeCount());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
