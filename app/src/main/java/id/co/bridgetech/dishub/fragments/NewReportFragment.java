package id.co.bridgetech.dishub.fragments;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.reflect.TypeToken;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Size;
import com.otaliastudios.cameraview.SizeSelector;

import java.util.ArrayList;
import java.util.List;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.activities.FinializeReportActivity;
import id.co.bridgetech.dishub.activities.MasterActivity;
import id.co.bridgetech.dishub.adapters.NotificationAdapter;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.interfaces.IEndlessScrollListener;
import id.co.bridgetech.dishub.interfaces.IPullToRefreshListener;
import id.co.bridgetech.dishub.models.Notification;
import id.co.bridgetech.dishub.services.AnimationManager;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.services.NavigationManager;
import id.co.bridgetech.dishub.utilities.EndlessScrollProvider;
import id.co.bridgetech.dishub.utilities.Formatter;
import id.co.bridgetech.dishub.utilities.ListProvider;

public class NewReportFragment extends Fragment {
    private MasterActivity context;
    private CameraView cameraView;
    private ImageView imageTakePhoto;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = BaseApplication.ACTIVITY;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_report, container, false);
//        cameraView = view.findViewById(R.id.camera);
//        imageTakePhoto = view.findViewById(R.id.image_take_photo);
//        cameraView.setLifecycleOwner(this.getViewLifecycleOwner());
//        cameraView.setCropOutput(true);
//        cameraView.setLayoutParams(new LinearLayout.LayoutParams(DisplayManager.getDisplay().x , DisplayManager.getDisplay().x));
//        cameraView.addCameraListener(new CameraListener() {
//            @Override
//            public void onPictureTaken(byte[] jpeg) {
//                cameraView.stop();
//                super.onPictureTaken(jpeg);
//
//                Bitmap img = Formatter.castToBitmap(jpeg);
//                img = Formatter.resizeBitmap(img,512,512);
//                byte[] imgArr = Formatter.castToBytes(img);
//                Intent intent = new Intent();
//                CacheManager.addTemporaryImageData(intent,imgArr,"data_PHOTO");
//                NavigationManager.openPageWithData(FinializeReportActivity.class,intent);
//
//            }
//        });
//
//        imageTakePhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AnimationManager.spring(imageTakePhoto);
//                cameraView.capturePicture();
//
//            }
//        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
//        cameraView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
//        cameraView.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        cameraView.destroy();
    }
}
