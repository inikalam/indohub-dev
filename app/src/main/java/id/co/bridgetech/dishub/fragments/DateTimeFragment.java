package id.co.bridgetech.dishub.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.interfaces.IDatePickerListener;

public class DateTimeFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private int month, year, day;
    private IDatePickerListener listener;

    public void setListener(IDatePickerListener listener) {
        this.listener = listener;
    }

    public DateTimeFragment() {
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        this.year = year;
        this.month = month;
        this.day = dayOfMonth;
        listener.setDateResult(year, month, day);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new DatePickerDialog(BaseApplication.ACTIVITY, this, year, month, day);
    }
}