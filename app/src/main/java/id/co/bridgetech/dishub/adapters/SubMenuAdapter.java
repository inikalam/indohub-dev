package id.co.bridgetech.dishub.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.models.Menu;
import id.co.bridgetech.dishub.models.SubMenu;
import id.co.bridgetech.dishub.services.MenuManager;
import id.co.bridgetech.dishub.services.NavigationManager;
import id.co.bridgetech.dishub.viewholders.MenuViewHolder;
import id.co.bridgetech.dishub.viewholders.SubMenuViewHolder;

public class SubMenuAdapter extends RecyclerView.Adapter<SubMenuViewHolder> {

    private List<SubMenu> data = new ArrayList<>();
    public SubMenuAdapter(List<id.co.bridgetech.dishub.models.SubMenu> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public SubMenuViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_menu, viewGroup, false);
        return new SubMenuViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final SubMenuViewHolder menuViewHolder, int i) {
        final id.co.bridgetech.dishub.models.SubMenu item = data.get(i);

        menuViewHolder.getImageThumbnail().setImageDrawable(BaseApplication.ACTIVITY.getResources().getDrawable(item.getImgResId()));
        menuViewHolder.getTextTitle().setText(item.getName());
        menuViewHolder.getLayoutRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.getListener().onSelected();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}

