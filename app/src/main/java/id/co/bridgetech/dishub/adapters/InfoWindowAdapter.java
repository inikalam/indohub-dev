package id.co.bridgetech.dishub.adapters;

import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.models.CCTV;
import id.co.bridgetech.dishub.models.Tenant;
import id.co.bridgetech.dishub.services.AnimationManager;
import id.co.bridgetech.dishub.services.CacheManager;

public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    TextView textTitle;
    TextView textAddress;
    //LinearLayout layoutOpen;
    //ImageView imageStatus;

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = BaseApplication.ACTIVITY.getLayoutInflater()
                .inflate(R.layout.info_window, null);

        //this.layoutOpen = view.findViewById(R.id.layout_open);
        this.textAddress = view.findViewById(R.id.text_address_info);
        this.textTitle = view.findViewById(R.id.text_title_info);
        //this.imageStatus = view.findViewById(R.id.image_status);


        CCTV item = null;
        if(marker.getTag() instanceof Tenant)
        {
            Tenant tenant = (Tenant) marker.getTag();
            textTitle.setText(tenant.getTitle());
            textAddress.setText(tenant.getAddress());
        }
        else if(marker.getTag() instanceof  CCTV) {
            item = (CCTV) marker.getTag();
            textTitle.setText(item.getTitle());
            textAddress.setText(item.getAddress());
        }
        else
        {
            textTitle.setText(CacheManager.getLocalProfile().getFullName());
            textAddress.setText("Mari disiplin berlalu-lintas");
        }
        return view;
    }
}