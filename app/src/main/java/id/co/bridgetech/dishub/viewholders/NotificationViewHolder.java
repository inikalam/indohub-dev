package id.co.bridgetech.dishub.viewholders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.bridgetech.dishub.R;

public class NotificationViewHolder extends RecyclerView.ViewHolder {
    private TextView textTitle;
    private TextView textMessage;
    private TextView textDate;
    private TextView textTime;
    private CircleImageView imageIcon;
    private LinearLayout layoutRoot;

    public TextView getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(TextView textTitle) {
        this.textTitle = textTitle;
    }

    public TextView getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(TextView textMessage) {
        this.textMessage = textMessage;
    }

    public TextView getTextDate() {
        return textDate;
    }

    public void setTextDate(TextView textDate) {
        this.textDate = textDate;
    }

    public TextView getTextTime() {
        return textTime;
    }

    public void setTextTime(TextView textTime) {
        this.textTime = textTime;
    }

    public CircleImageView getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(CircleImageView imageIcon) {
        this.imageIcon = imageIcon;
    }

    public LinearLayout getLayoutRoot() {
        return layoutRoot;
    }

    public void setLayoutRoot(LinearLayout layoutRoot) {
        this.layoutRoot = layoutRoot;
    }

    public NotificationViewHolder(@NonNull View itemView) {
        super(itemView);
        this.imageIcon = itemView.findViewById(R.id.image_icon);
        this.textDate = itemView.findViewById(R.id.text_date);
        this.textMessage = itemView.findViewById(R.id.text_message);
        this.textTime = itemView.findViewById(R.id.text_time);
        this.textTitle = itemView.findViewById(R.id.text_title);
        this.layoutRoot = itemView.findViewById(R.id.layout_root);
    }
}
