package id.co.bridgetech.dishub.services;

import android.content.Intent;
import android.net.Uri;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.utilities.AppInfo;

public class NavigationManager {
    public static void openGooglePlay()
    {
        try {
            BaseApplication.ACTIVITY.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + AppInfo.getPackage(BaseApplication.ACTIVITY))));
        } catch (android.content.ActivityNotFoundException anfe) {
            BaseApplication.ACTIVITY.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + AppInfo.getPackage(BaseApplication.ACTIVITY))));
        }
    }

    public static void openLink(String link)
    {
        try {
            BaseApplication.ACTIVITY.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
        } catch (android.content.ActivityNotFoundException anfe) {
        }
    }
    public static void openPage( Class<?> destination)
    {
        Intent intent = new Intent(BaseApplication.ACTIVITY,destination);
        BaseApplication.ACTIVITY.startActivity(intent);
    }

    public static void openPageWithData( Class<?> destination, Intent intent)
    {
        intent.setClass(BaseApplication.ACTIVITY,destination);
        BaseApplication.ACTIVITY.startActivity(intent);
    }
}
