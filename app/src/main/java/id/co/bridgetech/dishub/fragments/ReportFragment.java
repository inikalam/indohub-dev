package id.co.bridgetech.dishub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.activities.MasterActivity;
import id.co.bridgetech.dishub.adapters.NotificationAdapter;
import id.co.bridgetech.dishub.adapters.ReportAdapter;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.interfaces.IEndlessScrollListener;
import id.co.bridgetech.dishub.interfaces.IPullToRefreshListener;
import id.co.bridgetech.dishub.models.Notification;
import id.co.bridgetech.dishub.models.Report;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.utilities.EndlessScrollProvider;
import id.co.bridgetech.dishub.utilities.ListProvider;

public class ReportFragment extends Fragment {
    private RecyclerView recyclerView;
    private SwipeRefreshLayout layoutRefresh;
    private List<Report> reports;
    private MasterActivity context;
    private ReportAdapter reportAdapter;
    private EndlessScrollProvider provider;
    private String sinceId = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reports = new ArrayList<>();
        context = BaseApplication.ACTIVITY;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        context.initEmpty(view);
        layoutRefresh = view.findViewById(R.id.layout_refresh);
        recyclerView = view.findViewById(R.id.recycler_view);

        provider = ListProvider.getEndlessScrollProvider(recyclerView, new IEndlessScrollListener() {
            @Override
            public void onLoad(int index, int totalCount) {
                loadAll(index+1,false,false);
            }
        });

        reportAdapter = new ReportAdapter(reports);
        ListProvider.begin(recyclerView,reportAdapter,1);

        ListProvider.setPullToRefresh(layoutRefresh, new IPullToRefreshListener() {
            @Override
            public void onRefresh() {
                sinceId = null;
                loadAll(1,false,true);
            }
        });
        ListProvider.setEndlessScrolling(recyclerView,provider);
        loadAll(1,true,true);
        return view;
    }

    private void loadAll(int page, boolean loadFromCache , final boolean isReset)
    {
        context.HOST.getReports(page,sinceId, loadFromCache, new IApiListener() {
            @Override
            public void onSuccess(String result, String message) {
                List<Report> data = FormatManager.parseTo(new TypeToken<List<Report>>(){}.getType(),result);
                if(data != null) {
                    sinceId = message;
                    if(isReset)
                        reports.clear();
                    for (Report item : data) {
                        reports.add(item);
                    }
                    if(reports.size() == 0)
                    {
                        context.EMPTY.show(R.drawable.ic_post_it_128dp,"Belum ada data","Buatlah sebuah post untuk dapat dilihat semua orang");
                    }
                    else
                    {
                        reportAdapter.notifyDataSetChanged();
                        context.EMPTY.dismiss();
                    }
                }
                else
                {
                    //context.EMPTY.show(R.drawable.logo_indohub,"Belum ada data","Buatlah sebuah post untuk dapat dilihat semua orang");
                }
            }

            @Override
            public void onFailure(String message) {
                context.EMPTY.show(R.drawable.logo_indohub,"Oppppsss..",message);
            }

            @Override
            public void onStart(String message) {
                context.LOADER.show();
                context.EMPTY.dismiss();
            }

            @Override
            public void onEnd() {
                context.LOADER.dismiss();
                layoutRefresh.setRefreshing(false);
            }
        });
    }
}
