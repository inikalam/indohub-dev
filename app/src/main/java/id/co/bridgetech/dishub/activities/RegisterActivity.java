package id.co.bridgetech.dishub.activities;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.models.User;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.services.NavigationManager;
import id.co.bridgetech.dishub.services.PopupManager;
import id.co.bridgetech.dishub.utilities.AppInfo;
import id.co.bridgetech.dishub.utilities.Validator;

public class RegisterActivity extends MasterActivity {

    TextInputLayout layoutName;
    TextInputLayout layoutEmail;
    TextInputLayout layoutPassword;
    TextInputLayout layoutConfirmPassword;

    EditText inputName;
    EditText inputEmail;
    EditText inputPassword;
    EditText inputConfirmPassword;

    Button buttonSubmit;
    TextView textAppVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_register);
        initLoader();

        this.layoutName = findViewById(R.id.input_name);
        this.layoutEmail = findViewById(R.id.input_email);
        this.layoutPassword = findViewById(R.id.input_password);
        this.layoutConfirmPassword = findViewById(R.id.input_confirm_password);
        this.inputName = findViewById(R.id.text_name);
        this.inputEmail = findViewById(R.id.text_email);
        this.inputConfirmPassword = findViewById(R.id.text_confirm_password);
        this.inputPassword = findViewById(R.id.text_password);
        this.buttonSubmit = findViewById(R.id.button_submit);
        this.textAppVersion = findViewById(R.id.app_version);

        textAppVersion.setText("App version v." + AppInfo.getVersion(this));

        layoutName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                removeError();
                return false;
            }
        });

        layoutEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                removeError();
                return false;
            }
        });

        layoutPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                removeError();
                return false;
            }
        });

        layoutConfirmPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                removeError();
                return false;
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeError();
                if(!Validator.isNotEmpty(layoutName))return;
                if(!Validator.isMinLengthValid(layoutName,3))return;
                if(!Validator.isMaxLengthValid(layoutName,50))return;
                if(!Validator.isNotEmpty(layoutEmail))return;
                if(!Validator.isMinLengthValid(layoutEmail,5))return;
                if(!Validator.isMaxLengthValid(layoutEmail,50))return;
                if(!Validator.isMailValid(layoutEmail))return;
                if(!Validator.isNotEmpty(layoutPassword))return;
                if(!Validator.isMinLengthValid(layoutPassword,6))return;
                if(!Validator.isMaxLengthValid(layoutPassword,25))return;
                if(!Validator.isNotEmpty(layoutConfirmPassword))return;
                if(!Validator.isMinLengthValid(layoutConfirmPassword,6))return;
                if(!Validator.isMaxLengthValid(layoutConfirmPassword,25))return;
                if(!Validator.isSameWith(layoutPassword,layoutConfirmPassword))return;
                if(!Validator.isMatchPasswordRequirements(layoutPassword))return;

                HOST.register(inputName.getText().toString(), inputEmail.getText().toString(), inputPassword.getText().toString(), new IApiListener() {
                    @Override
                    public void onSuccess(String result, String message) {
                        User data = FormatManager.parseTo(User.class,result);
                        if(data.isConfirmed()) {
                            CacheManager.saveLocalProfile(data);
                            PopupManager.createAnnouncement("Selamat Datang", message);
                            NavigationManager.openPage(MainActivity.class);
                            finish();
                        }
                        else
                        {
                            Intent intent = new Intent();
                            CacheManager.addTemporaryData(intent,data,"data_USER");
                            NavigationManager.openPageWithData(VerificationActivity.class,intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        PopupManager.showToast(message);
                    }

                    @Override
                    public void onStart(String message) {
                        LOADER.show();
                    }

                    @Override
                    public void onEnd() {
                        LOADER.dismiss();
                    }
                });
            }
        });
    }

    private void removeError()
    {
        layoutConfirmPassword.setError(null);
        layoutPassword.setError(null);
        layoutEmail.setError(null);
        layoutName.setError(null);
    }
}
