package id.co.bridgetech.dishub.models;

public class Auth {
    private String accessToken;
    private String encryptedAccessToken;
    private int expireInSecond;
    private int userId;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getEncryptedAccessToken() {
        return encryptedAccessToken;
    }

    public void setEncryptedAccessToken(String encryptedAccessToken) {
        this.encryptedAccessToken = encryptedAccessToken;
    }

    public int getExpireInSecond() {
        return expireInSecond;
    }

    public void setExpireInSecond(int expireInSecond) {
        this.expireInSecond = expireInSecond;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
