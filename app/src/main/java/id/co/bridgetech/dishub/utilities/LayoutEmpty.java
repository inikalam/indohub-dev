package id.co.bridgetech.dishub.utilities;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tuyenmonkey.mkloader.MKLoader;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.interfaces.INotifierListener;
import id.co.bridgetech.dishub.services.AnimationManager;
import id.co.bridgetech.dishub.services.LogManager;

public class LayoutEmpty {
    private Activity context;
    private RelativeLayout overlay;
    private TextView textTitle;
    private TextView textMessage;
    private LinearLayout layoutImage;
    private ImageView imagePresenter;


    public LayoutEmpty(View view) {
        this.context = BaseApplication.ACTIVITY;
        this.overlay = view.findViewById(R.id.progress_overlay_empty);
        this.imagePresenter = view.findViewById(R.id.image_presenter_empty);
        this.textMessage = view.findViewById(R.id.text_message_empty);
        this.textTitle = view.findViewById(R.id.text_title_empty);
        this.layoutImage = view.findViewById(R.id.layout_image_empty);
        dismiss();
    }

    public LayoutEmpty() {
        this.context = BaseApplication.ACTIVITY;
        this.overlay = context.findViewById(R.id.progress_overlay_empty);
        this.imagePresenter = context.findViewById(R.id.image_presenter_empty);
        this.textMessage = context.findViewById(R.id.text_message_empty);
        this.textTitle = context.findViewById(R.id.text_title_empty);
        this.layoutImage = context.findViewById(R.id.layout_image_empty);
        dismiss();
    }

    public void show(int imageResId, String title , String message)
    {
        try {
            this.imagePresenter.setImageDrawable(context.getDrawable(imageResId));
            this.textTitle.setText(title);
            this.textMessage.setText(message);
            this.layoutImage.setVisibility(View.VISIBLE);
            this.overlay.setVisibility(View.VISIBLE);
            AnimationManager.spring(imagePresenter);
        }catch (Exception ex){
            LogManager.record("SHOW_NOTIFIER",ex.getMessage());
        }
    }

    public void dismiss()
    {
        try {
            this.layoutImage.setVisibility(View.INVISIBLE);
            this.overlay.setVisibility(View.INVISIBLE);
        }catch (Exception ex){}
    }
}
