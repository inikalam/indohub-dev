package id.co.bridgetech.dishub.interfaces;

public interface IDatePickerListener {
    void setDateResult(int year , int month , int day);
}
