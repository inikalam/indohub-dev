package id.co.bridgetech.dishub.viewholders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.bridgetech.dishub.R;

public class ChatViewHolder extends RecyclerView.ViewHolder {

    private CircleImageView imageProfile;
    private TextView textName;
    private TextView textMessage;
    private TextView textDate;

    public ChatViewHolder(@NonNull View itemView) {
        super(itemView);
        this.imageProfile = itemView.findViewById(R.id.image_profile);
        this.textDate = itemView.findViewById(R.id.text_date);
        this.textMessage = itemView.findViewById(R.id.text_message);
        this.textName = itemView.findViewById(R.id.text_name);
    }

    public CircleImageView getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(CircleImageView imageProfile) {
        this.imageProfile = imageProfile;
    }

    public TextView getTextName() {
        return textName;
    }

    public void setTextName(TextView textName) {
        this.textName = textName;
    }

    public TextView getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(TextView textMessage) {
        this.textMessage = textMessage;
    }

    public TextView getTextDate() {
        return textDate;
    }

    public void setTextDate(TextView textDate) {
        this.textDate = textDate;
    }
}
