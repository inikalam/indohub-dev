package id.co.bridgetech.dishub.services;

import android.net.Uri;
import android.os.Handler;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.models.CCTV;

public class MediaPlayerManager {
    SimpleExoPlayer player;
    SimpleExoPlayerView video;
    CCTV cctv;
    DefaultDataSourceFactory dataSourceFactory;
    HlsMediaSource mediaSource;
    Handler handler = new Handler();

    public MediaPlayerManager(SimpleExoPlayerView video, CCTV cctv) {
        this.video = video;
        this.cctv = cctv;
    }

    public void start()
    {
        player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(BaseApplication.ACTIVITY),new DefaultTrackSelector(),new DefaultLoadControl());
        Uri uri = Uri.parse(cctv.getLink());
        video.setPlayer(player);
        dataSourceFactory = new DefaultDataSourceFactory(BaseApplication.ACTIVITY, "user-agent");
        mediaSource = new HlsMediaSource(uri, dataSourceFactory, handler, null);
        player.prepare(mediaSource);
        player.setPlayWhenReady(true);
    }

    public void stop()
    {
        player.setPlayWhenReady(false);
        player.release();
    }
}
