package id.co.bridgetech.dishub.utilities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.DateFormat;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class Formatter {
    public static Bitmap castToBitmap(byte[] image)
    {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public static Bitmap resizeBitmap(Bitmap bitmap , int width , int height)
    {
        return Bitmap.createScaledBitmap(bitmap, width, height, false);
    }

    public static byte[] castToBytes(Bitmap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
        return bos.toByteArray();
    }

    public static String castToRupiah(double amount)
    {
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);
        String result = kursIndonesia.format(amount);
        return result.substring(0,result.length()-3);
    }

    public static String castToFormattedDate(String dateString)
    {
        String[] arr = dateString.split("T");
        String[] dateArr = arr[0].split("-");
        return dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];
    }

    public static String castToFormattedDate(long num)
    {
       return DateFormat.format("dd/MM/yyyy", new Date(num)).toString();
    }

    public static String castToFormattedTime(String dateString)
    {
        String[] arr = dateString.split("T");
        String[] timeArr = arr[1].split(":");
        return timeArr[0] + ":" + timeArr[1];
    }

    public static String castToFormattedTime(long num)
    {
        return DateFormat.format("hh:mm", new Date(num)).toString();
    }
}
