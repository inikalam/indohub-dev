package id.co.bridgetech.dishub.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.fragments.DashboardFragment;
import id.co.bridgetech.dishub.fragments.NewReportFragment;
import id.co.bridgetech.dishub.fragments.NotificationFragment;
import id.co.bridgetech.dishub.fragments.ProfileFragment;
import id.co.bridgetech.dishub.fragments.ReportFragment;
import id.co.bridgetech.dishub.services.DisplayManager;

public class MainActivity extends MasterActivity {

    DashboardFragment dashboard;
    NotificationFragment notification;
    ReportFragment report;
    ProfileFragment profile;
    NewReportFragment newReport;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    dashboard = dashboard == null ? new DashboardFragment() : dashboard;
                    loadFragment(dashboard);
                    return true;
                case R.id.navigation_report:
                    report = report == null ? new ReportFragment() : report;
                    loadFragment(report);
                    return true;
                case R.id.navigation_new_report:
                    newReport = newReport == null ? new NewReportFragment() : newReport;
                    loadFragment(newReport);
                    return true;
                case R.id.navigation_notification:
                    notification = notification == null ? new NotificationFragment() : notification;
                    loadFragment(notification);
                    return true;
                case R.id.navigation_profile:
                    profile = profile == null ? new ProfileFragment() : profile;
                    loadFragment(profile);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_main);
        initLoader();

        BottomNavigationView navigation = (BottomNavigationView)findViewById(R.id.navigation_main);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        dashboard = new DashboardFragment();
        loadFragment(dashboard);


    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {

    }
}
