package id.co.bridgetech.dishub.fragments;

import android.app.NotificationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.synnapps.carouselview.CarouselView;

import java.util.ArrayList;
import java.util.List;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.activities.MasterActivity;
import id.co.bridgetech.dishub.adapters.MenuAdapter;
import id.co.bridgetech.dishub.adapters.NotificationAdapter;
import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.interfaces.IEndlessScrollListener;
import id.co.bridgetech.dishub.interfaces.IPullToRefreshListener;
import id.co.bridgetech.dishub.models.Banner;
import id.co.bridgetech.dishub.models.Notification;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.services.PopupManager;
import id.co.bridgetech.dishub.utilities.EndlessScrollProvider;
import id.co.bridgetech.dishub.utilities.ListProvider;

public class NotificationFragment extends Fragment {
    private RecyclerView recyclerView;
    private SwipeRefreshLayout layoutRefresh;
    private List<Notification> notifications;
    private MasterActivity context;
    private NotificationAdapter notificationAdapter;
    private EndlessScrollProvider provider;
    private String sinceId = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notifications = new ArrayList<>();
        context = BaseApplication.ACTIVITY;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        context.initEmpty(view);
        layoutRefresh = view.findViewById(R.id.layout_refresh);
        recyclerView = view.findViewById(R.id.recycler_view);

        provider = ListProvider.getEndlessScrollProvider(recyclerView, new IEndlessScrollListener() {
            @Override
            public void onLoad(int index, int totalCount) {
                loadAll(index+1,false,false);
            }
        });

        notificationAdapter = new NotificationAdapter(notifications);
        ListProvider.begin(recyclerView,notificationAdapter,1);

        ListProvider.setPullToRefresh(layoutRefresh, new IPullToRefreshListener() {
            @Override
            public void onRefresh() {
                sinceId = null;
                loadAll(1,false,true);
            }
        });
        ListProvider.setEndlessScrolling(recyclerView,provider);
        loadAll(1,true,true);
        return view;
    }

    private void loadAll(int page, boolean loadFromCache , final boolean isReset)
    {
        context.HOST.getNotification(page,sinceId, loadFromCache, new IApiListener() {
            @Override
            public void onSuccess(String result, String message) {
                List<Notification> data = FormatManager.parseTo(new TypeToken<List<Notification>>(){}.getType(),result);
                if(data != null) {
                    sinceId = message;
                    if(isReset)
                        notifications.clear();
                    for (Notification item : data) {
                        notifications.add(item);
                    }
                    if(notifications.size() == 0)
                    {
                        context.EMPTY.show(R.drawable.ic_email_128dp,"Belum ada data","Berinteraksilah menggunakan aplikasi ini untuk memperoleh notifikasi");
                    }
                    else
                    {
                        notificationAdapter.notifyDataSetChanged();
                        context.EMPTY.dismiss();
                    }
                }
                else
                {
                    //context.EMPTY.show(R.drawable.logo_indohub,"Belum ada data","berinteraksilah menggunakan aplikasi ini untuk memperoleh notifikasi");
                }
            }

            @Override
            public void onFailure(String message) {
                context.EMPTY.show(R.drawable.ic_email_128dp,"Oppppsss..",message);
                //PopupManager.showToast(message);
            }

            @Override
            public void onStart(String message) {
                context.LOADER.show();
                context.EMPTY.dismiss();
            }

            @Override
            public void onEnd() {
                context.LOADER.dismiss();
                layoutRefresh.setRefreshing(false);
            }
        });
    }
}
