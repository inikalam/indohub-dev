package id.co.bridgetech.dishub.utilities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.interfaces.IEndlessScrollListener;
import id.co.bridgetech.dishub.interfaces.IPullToRefreshListener;

public class ListProvider {
    public static void begin(RecyclerView recyclerView, RecyclerView.Adapter adapter, int column)
    {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(BaseApplication.ACTIVITY, column);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridDecorator(column,GridDecorator.dpToPx(2), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    public static EndlessScrollProvider getEndlessScrollProvider(RecyclerView recyclerView, final IEndlessScrollListener listener)
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(BaseApplication.ACTIVITY);
        recyclerView.setLayoutManager(linearLayoutManager);
        return new EndlessScrollProvider(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                listener.onLoad(page,totalItemsCount);
            }
        };
    }

    public static void setEndlessScrolling(RecyclerView recyclerView,EndlessScrollProvider provider)
    {
        recyclerView.addOnScrollListener(provider);
    }

    public static <T> void setPullToRefresh(final SwipeRefreshLayout layoutRefresh, final IPullToRefreshListener listener)
    {
        layoutRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                layoutRefresh.setRefreshing(true);
                listener.onRefresh();
            }
        });
    }
}
