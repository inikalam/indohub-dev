package id.co.bridgetech.dishub.interfaces;

import com.google.android.gms.maps.model.Marker;

public interface IInfoWindowClickListener {
    void openMarker(Marker marker);
}
