package id.co.bridgetech.dishub.services;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.models.Auth;
import id.co.bridgetech.dishub.models.User;
import id.co.bridgetech.dishub.utilities.Routes;

public class CacheManager {


    public static String getTemporaryData(String key)
    {

        return BaseApplication.ACTIVITY.getIntent().getExtras().getString(key);
    }

    public static <T> T getTemporaryData(String key,Class<T> type)
    {
        return FormatManager.parseTo(type,getTemporaryData(key));
    }

    public static void addTemporaryData(Intent intent, Object data, String key)
    {
        intent.putExtra(key,new Gson().toJson(data));
    }
    public static byte[] getTemporaryImageData(String key)
    {

        return BaseApplication.ACTIVITY.getIntent().getExtras().getByteArray(key);
    }

    public static void addTemporaryImageData(Intent intent, byte[] data, String key)
    {
        intent.putExtra(key,data);
    }

    public static void save(String key, String value)
    {
        try {
            key = key.replace("/","").replace("=","").replace("?","");
            String encrypted = CryptoManager.encrypt(value);
            FileOutputStream outputStream = BaseApplication.ACTIVITY.openFileOutput(key, Context.MODE_PRIVATE);
            outputStream.write(encrypted.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static String load(String key)
    {
        key = key.replace("/","").replace("=","").replace("?","");
        File directory = BaseApplication.ACTIVITY.getFilesDir();
        File file = new File(directory, key);
        try {
            FileInputStream fin = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            reader.close();
            String decrypted = CryptoManager.decrypt(sb.toString());
            return decrypted;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        fileOrDirectory.delete();
    }

    public static void clearAllCache()
    {
        deleteRecursive(BaseApplication.ACTIVITY.getFilesDir());
    }

    public static void saveLocalProfile(User data)
    {
        save(Routes.USER_LOGIN,new Gson().toJson(data));
    }

    public static User getLocalProfile()
    {
        String data = load(Routes.USER_LOGIN);
        User result = null;
        try {
            result = new Gson().fromJson(data,new TypeToken<User>() {}.getType());
        }catch (Exception ex)
        {}
        return result;
    }

    public static void saveAuth(Auth data)
    {
        save(Routes.TOKEN_AUTH,new Gson().toJson(data));
    }

    public static Auth getAuth()
    {
        String data = load(Routes.TOKEN_AUTH);
        Auth result = null;
        try {
            result = new Gson().fromJson(data,new TypeToken<Auth>() {}.getType());
        }catch (Exception ex)
        {}
        return result;
    }

    public static void clearProfile()
    {
        File directory = BaseApplication.ACTIVITY.getFilesDir();
        File file = new File(directory, Routes.USER_LOGIN);
        file.delete();
    }

    public static void clearAuth()
    {
        File directory = BaseApplication.ACTIVITY.getFilesDir();
        File file = new File(directory, Routes.TOKEN_AUTH);
        file.delete();
    }

    public static boolean isAlreadyLogin()
    {
        return getLocalProfile() != null;
    }
}
