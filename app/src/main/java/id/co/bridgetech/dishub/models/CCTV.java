package id.co.bridgetech.dishub.models;

public class CCTV {

    private String title;
    private String address;
    private String link;
    private String type;
    private boolean firstChat;
    private String groupChatCode;
    private String broadcast;
    private String id;
    private Coordinate coordinate;

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isFirstChat() {
        return firstChat;
    }

    public void setFirstChat(boolean firstChat) {
        this.firstChat = firstChat;
    }

    public String getGroupChatCode() {
        return groupChatCode;
    }

    public void setGroupChatCode(String groupChatCode) {
        this.groupChatCode = groupChatCode;
    }

    public String getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(String broadcast) {
        this.broadcast = broadcast;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public class Coordinate
    {
        private Double longitude;
        private Double latitude;

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }
    }
}
