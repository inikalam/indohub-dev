package id.co.bridgetech.dishub.interfaces;

public interface ISubMenuListener {
    void onSelected();
}
