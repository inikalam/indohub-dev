package id.co.bridgetech.dishub.services;

import android.graphics.Point;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import id.co.bridgetech.dishub.BaseApplication;

public class DisplayManager {
    public static void setFullScreen()
    {
        //SET PAGE TO FULL SIZE MODE//
        BaseApplication.ACTIVITY.requestWindowFeature(Window.FEATURE_NO_TITLE);
        BaseApplication.ACTIVITY.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static Point getDisplay()
    {
        Display display = BaseApplication.ACTIVITY.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static float getDensity()
    {
        return BaseApplication.ACTIVITY.getResources().getDisplayMetrics().density;
    }

}
