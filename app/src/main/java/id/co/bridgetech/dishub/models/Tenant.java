package id.co.bridgetech.dishub.models;

import java.util.List;

public class Tenant {
    private String title;
    private String address;
    private String openHour;
    private String closeHour;
    private String imageUrl;
    private List<Child> listTenant;
    private String id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOpenHour() {
        return openHour;
    }

    public void setOpenHour(String openHour) {
        this.openHour = openHour;
    }

    public String getCloseHour() {
        return closeHour;
    }

    public void setCloseHour(String closeHour) {
        this.closeHour = closeHour;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Child> getListTenant() {
        return listTenant;
    }

    public void setListTenant(List<Child> listTenant) {
        this.listTenant = listTenant;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static class Child
    {
        private String id;
        private String title;
        private String link;
        private String phoneNumber;
        private String type;
        private boolean firstChat;
        private String groupChatCode;
        private String broadcast;
        private CCTV.Coordinate coordinate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public boolean isFirstChat() {
            return firstChat;
        }

        public void setFirstChat(boolean firstChat) {
            this.firstChat = firstChat;
        }

        public String getGroupChatCode() {
            return groupChatCode;
        }

        public void setGroupChatCode(String groupChatCode) {
            this.groupChatCode = groupChatCode;
        }

        public String getBroadcast() {
            return broadcast;
        }

        public void setBroadcast(String broadcast) {
            this.broadcast = broadcast;
        }

        public CCTV.Coordinate getCoordinate() {
            return coordinate;
        }

        public void setCoordinate(CCTV.Coordinate coordinate) {
            this.coordinate = coordinate;
        }
    }
}
