package id.co.bridgetech.dishub.services;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.R;
import id.co.bridgetech.dishub.interfaces.IChatListener;
import id.co.bridgetech.dishub.models.Chat;
import id.co.bridgetech.dishub.models.User;

public class ChatManager {
    FirebaseDatabase database;
    DatabaseReference dbReference;
    FirebaseOptions options;
    String room = null;
    String subPath = "messages/";
    User user;

    private final String appId = "3b33d9b79ee1b800";
    private final String apiKey = "AIzaSyDCo6qg2h8ztMeoi21YHE-rIBk-X8X-VM0";
    private final String databaseUrl = "https://indo-hub.firebaseio.com";
    private final String authDomain = "indo-hub.firebaseapp.com";
    private final String storageBucket = "indo-hub.appspot.com";
    private final String messagingSenderId = "1029073187282";
    private final String trackingId = "647431854232";
    private final String projectId = "indo-hub";
    private EditText inputMessage;
    private ImageView imageSend;

    public void initChat(String id, final IChatListener listener)
    {

        user = CacheManager.getLocalProfile();
        inputMessage = BaseApplication.ACTIVITY.findViewById(R.id.input_chat_message);
        imageSend = BaseApplication.ACTIVITY.findViewById(R.id.image_send);
        try {
            options = new FirebaseOptions.Builder().setApiKey(apiKey).setApplicationId(appId).setDatabaseUrl(databaseUrl).setGaTrackingId(trackingId).setGcmSenderId(messagingSenderId).setProjectId(projectId).setStorageBucket(storageBucket).build();
            FirebaseApp.initializeApp(BaseApplication.ACTIVITY, options);
        }catch (Exception e){}
        this.room = subPath + id;
        database = FirebaseDatabase.getInstance();
        dbReference = database.getReference(room);

        dbReference.limitToLast(20).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                listener.chatAdded(FormatManager.parseTo(Chat.class,new Gson().toJson(dataSnapshot.getValue())));
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        imageSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = inputMessage.getText().toString().trim();
                AnimationManager.spring(imageSend);
                if(!msg.isEmpty()) {
                    inputMessage.setText("");
                    Chat newChat = new Chat();
                    newChat.setText(msg);
                    newChat.setTimestamp(ServerValue.TIMESTAMP);
                    Chat.FirebaseUser newUser = new Chat.FirebaseUser();
                    newUser.setFullName(user.getFullName());
                    newUser.setImageUrl(user.getImageUrl());
                    newChat.setUser(newUser);
                    dbReference.push().setValue(newChat);
                }
            }
        });
    }
    public void dismissChat()
    {

    }
}
