package id.co.bridgetech.dishub.interfaces;

public interface IEndlessScrollListener {
    void onLoad(int index,int totalCount);
}
