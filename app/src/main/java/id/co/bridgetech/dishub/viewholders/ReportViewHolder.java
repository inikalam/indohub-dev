package id.co.bridgetech.dishub.viewholders;

import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.bridgetech.dishub.R;

public class ReportViewHolder extends RecyclerView.ViewHolder  {
    private ImageView imagePosting;
    private ImageView imageLike;
    private ImageView imageComment;
    private LinearLayout layoutLocation;
    private CircleImageView imageProfile;
    private TextView textLike;
    private TextView textComment;
    private TextView textDescription;
    private TextView textName;
    private TextView textDate;
    private TextView textTime;
    private TextView textLocation;

    public ReportViewHolder(@NonNull View itemView) {
        super(itemView);
        this.imagePosting = itemView.findViewById(R.id.image_posting);
        this.imageComment = itemView.findViewById(R.id.image_comment);
        this.imageLike = itemView.findViewById(R.id.image_like);
        this.imageProfile = itemView.findViewById(R.id.image_profile);
        this.textComment = itemView.findViewById(R.id.text_comment);
        this.textLike = itemView.findViewById(R.id.text_like);
        this.textDescription = itemView.findViewById(R.id.text_description);
        this.textName = itemView.findViewById(R.id.text_name);
        this.textDate = itemView.findViewById(R.id.text_date);
        this.textTime = itemView.findViewById(R.id.text_time);
        this.textLocation = itemView.findViewById(R.id.text_location);
        this.layoutLocation = itemView.findViewById(R.id.layout_location);
    }

    public LinearLayout getLayoutLocation() {
        return layoutLocation;
    }

    public void setLayoutLocation(LinearLayout layoutLocation) {
        this.layoutLocation = layoutLocation;
    }

    public TextView getTextLocation() {
        return textLocation;
    }

    public void setTextLocation(TextView textLocation) {
        this.textLocation = textLocation;
    }

    public ImageView getImagePosting() {
        return imagePosting;
    }

    public void setImagePosting(ImageView imagePosting) {
        this.imagePosting = imagePosting;
    }

    public ImageView getImageLike() {
        return imageLike;
    }

    public void setImageLike(ImageView imageLike) {
        this.imageLike = imageLike;
    }

    public ImageView getImageComment() {
        return imageComment;
    }

    public void setImageComment(ImageView imageComment) {
        this.imageComment = imageComment;
    }

    public TextView getTextLike() {
        return textLike;
    }

    public void setTextLike(TextView textLike) {
        this.textLike = textLike;
    }

    public TextView getTextComment() {
        return textComment;
    }

    public void setTextComment(TextView textComment) {
        this.textComment = textComment;
    }

    public TextView getTextDescription() {
        return textDescription;
    }

    public void setTextDescription(TextView textDescription) {
        this.textDescription = textDescription;
    }

    public CircleImageView getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(CircleImageView imageProfile) {
        this.imageProfile = imageProfile;
    }

    public TextView getTextName() {
        return textName;
    }

    public void setTextName(TextView textName) {
        this.textName = textName;
    }

    public TextView getTextDate() {
        return textDate;
    }

    public void setTextDate(TextView textDate) {
        this.textDate = textDate;
    }

    public TextView getTextTime() {
        return textTime;
    }

    public void setTextTime(TextView textTime) {
        this.textTime = textTime;
    }
}
