package id.co.bridgetech.dishub.services;

import android.content.Intent;
import android.net.Uri;

import id.co.bridgetech.dishub.BaseApplication;
import id.co.bridgetech.dishub.activities.PostDamkarActivity;

public class CamManager {

    public static void cropCapturedImage(Uri picUri){
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(picUri, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", 256);
        cropIntent.putExtra("outputY", 256);
        cropIntent.putExtra("return-data", true);
        BaseApplication.ACTIVITY.startActivityForResult(cropIntent, 2);
    }
}
