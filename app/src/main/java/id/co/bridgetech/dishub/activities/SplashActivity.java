package id.co.bridgetech.dishub.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import id.co.bridgetech.dishub.interfaces.IApiListener;
import id.co.bridgetech.dishub.models.Auth;
import id.co.bridgetech.dishub.services.CacheManager;
import id.co.bridgetech.dishub.services.DisplayManager;
import id.co.bridgetech.dishub.services.FormatManager;
import id.co.bridgetech.dishub.services.NavigationManager;

public class SplashActivity extends MasterActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        initLoader();
        HOST.initAuth(new IApiListener() {
            @Override
            public void onSuccess(String result, String message) {
                Auth auth = FormatManager.parseTo(Auth.class,result);
                CacheManager.saveAuth(auth);
                if(CacheManager.isAlreadyLogin())
                {
                    NavigationManager.openPage(MainActivity.class);
                }
                else
                {
                    NavigationManager.openPage(LoginActivity.class);
                }
                finish();
            }

            @Override
            public void onFailure(String message) {
                NavigationManager.openPage(LoginActivity.class);
                finish();
            }

            @Override
            public void onStart(String message) {

            }

            @Override
            public void onEnd() {

            }
        });
    }
}
